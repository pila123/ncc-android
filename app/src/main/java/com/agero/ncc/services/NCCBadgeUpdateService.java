package com.agero.ncc.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.JobIntentService;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.agero.ncc.utils.NccConstants;

import co.chatsdk.core.dao.Thread;
import co.chatsdk.core.events.NetworkEvent;
import co.chatsdk.core.interfaces.ThreadType;
import co.chatsdk.core.session.NM;
import timber.log.Timber;

public class NCCBadgeUpdateService extends JobIntentService {

    public static int mOldBadgeCount, mOldMessageCount;
    public static String mRegisterThreadName = "";
//    public static boolean mIsFirstTime;
//    public NCCBadgeUpdateService() {
//        super("NCCBadgeUpdateService");
//    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        sendBroadCast();
    }

//    @Override
//    protected void onHandleIntent(@Nullable Intent intent) {
//        sendBroadCast();
//    }

    public void sendBroadCast() {
        int count = 0, messageCount = 0;
        if(NM.a()!= null && NM.thread() != null) {
            for (Thread thread :
                    NM.thread().getThreads(ThreadType.Private, false)) {
                if (!thread.isLastMessageWasRead() && (isIndivualChat(thread) || isTodayConversation(thread))) {
                    count += 1;
                    messageCount += thread.getUnreadMessagesCount();

                }
            }
            if (mOldBadgeCount != count) {
//                mIsFirstTime = false;
                mOldBadgeCount = count;
                mOldMessageCount = messageCount;
                Intent gcm_rec = new Intent(NccConstants.CHAT_ACTION);
                gcm_rec.putExtra(NccConstants.CHAT_BADGE_COUNT, count);
                LocalBroadcastManager.getInstance(this).sendBroadcast(gcm_rec);
            } else if (mOldMessageCount != messageCount) {
                mOldMessageCount = messageCount;
                Intent gcm_rec = new Intent(NccConstants.CHAT_ACTION);
                gcm_rec.putExtra(NccConstants.CHAT_MESSAGE_COUNT, messageCount);
                LocalBroadcastManager.getInstance(this).sendBroadcast(gcm_rec);
            }
        }
    }

     public static boolean isIndivualChat(Thread thread) {

        return !(thread != null && thread.getName() != null && thread.getName().toLowerCase().startsWith("job #"));
    }

    public static boolean isTodayConversation(Thread thread) {
        boolean isTodayConversation = false;
        long currentTime = System.currentTimeMillis();
        long messageTime = thread.getLastMessageAddedDate().getTime();
        //24hrs in millis
        long dayInMillis = 86400000;
        if ((currentTime - messageTime) < dayInMillis) {
            isTodayConversation = true;
        }
        return isTodayConversation;
    }

}
