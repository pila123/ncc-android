package com.agero.ncc.adapter;


import android.content.Context;
import android.content.SharedPreferences;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.model.JobDetail;
import com.agero.ncc.utils.DateTimeUtils;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.UiUtils;
import com.agero.ncc.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.agero.ncc.utils.LogUtilsKt.debug;

public class ActiveJobsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_ITEM = 2;
    private Context mContext;
    private ArrayList<JobDetail> jobList;
    private SharedPreferences mPrefs;
    private int mSelectedJobPosition;


    public ActiveJobsAdapter(Context mContext, ArrayList<JobDetail> statuses, SharedPreferences mPrefs, int selectedJobPosition) {
        this.mContext = mContext;
        this.jobList = statuses;
        this.mPrefs = mPrefs;
        this.mSelectedJobPosition = selectedJobPosition;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.active_job_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if(jobList.size() < 1 || position <0){
            return;
        }

        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
        if(mContext.getResources().getBoolean(R.bool.isTablet)) {
            itemViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mSelectedJobPosition = position;
                    notifyDataSetChanged();
                }
            });

            if (mSelectedJobPosition == position) {
                itemViewHolder.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.tab_tap_color));
            } else {
                itemViewHolder.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.ncc_white));
            }
        }
        try {
            JobDetail job = jobList.get(holder.getAdapterPosition());
            debug(ActiveJobsAdapter.class, " PONumber: " + job.getPoNumber());

            itemViewHolder.mTextActiveJobTitle.setText(Utils.getDisplayServiceTypes(mContext,job.getServiceTypeCode()) + " - " + job.getVehicle().getYear()
                    + " " + job.getVehicle().getMake() + " " + job.getVehicle().getModel() + " " + job.getVehicle().getColor());
            itemViewHolder.mTextActiveJobDisablementAddress.setText(Utils.toCamelCase(job.getDisablementLocation().getAddressLine1() + ", "
                    + job.getDisablementLocation().getCity() + ", ") + job.getDisablementLocation().getState());
            String statusText = Utils.getDisplayStatusText(mContext, job.getCurrentStatus().getStatusCode());
            itemViewHolder.mTextActiveJobStatus.setText("Status: " + statusText);
            itemViewHolder.mJobId.setText(UiUtils.getJobIdDisplayFormat(job.getDispatchId()));
            //if((statusText.toLowerCase().contains("pending") || statusText.toLowerCase().contains("goa") || statusText.toLowerCase().contains("unsuccessful")|| statusText.toLowerCase().contains("cancel")) && job.getCurrentStatus().isPending()){
            if (job.getCurrentStatus().getIsPending() != null && job.getCurrentStatus().getIsPending()) {
                itemViewHolder.mTextActiveJobStatus.setTextColor(mContext.getResources()
                        .getColor(R.color.jobdetail_pending_color));
                if(job.getCurrentStatus().getStatusCode().equalsIgnoreCase(NccConstants.JOB_STATUS_ACCEPTED)){
                    itemViewHolder.mTextActiveJobStatus.setText("Status: " + statusText + "...");
                }else {
                    itemViewHolder.mTextActiveJobStatus.setText("Status: " + mContext.getString(R.string.text_pending) + " " + statusText + "...");
                }
            }else if(job.getCurrentStatus().getIsJobOnHold() != null && job.getCurrentStatus().getIsJobOnHold()){
                itemViewHolder.mTextActiveJobStatus.setText("Status: " + mContext.getString(R.string.status_on_hold));
                itemViewHolder.mTextActiveJobStatus.setTextColor(mContext.getResources()
                        .getColor(R.color.jobdetail_pending_color));
            }
            else {
                itemViewHolder.mTextActiveJobStatus.setTextColor(mContext.getResources()
                        .getColor(R.color.jobdetail_heading_color));
            }
            if (Utils.isTow(job.getServiceTypeCode())) {
                itemViewHolder.mTextActiveJobTowToAddress.setText(Utils.toCamelCase(job.getTowLocation().getAddressLine1() + ", "
                        + job.getTowLocation().getCity() + ", ") + job.getTowLocation().getState());
                itemViewHolder.mConstraintTowTo.setVisibility(View.VISIBLE);
                itemViewHolder.mTextVerticalLineDesablement.setVisibility(View.VISIBLE);
                itemViewHolder.mViewStatusDisablementDivider.setVisibility(View.VISIBLE);
            } else {
                itemViewHolder.mConstraintTowTo.setVisibility(View.GONE);
                itemViewHolder.mTextVerticalLineDesablement.setVisibility(View.GONE);
                itemViewHolder.mViewStatusDisablementDivider.setVisibility(View.GONE);
            }


            if (!TextUtils.isEmpty(job.getEtaDateTime())) {
                String dateTime;
                if (job.getEtaExtensionCount() == null || job.getEtaExtensionCount() == 0) {
                    dateTime = DateTimeUtils.getEtaDateTime(job.getEtaDateTime(), job.getQuotedEta(), false,job.getDisablementLocation().getTimeZone());
                } else {
                    dateTime = DateTimeUtils.getEtaDateTime(job.getEtaDateTime(), job.getTotalEtaInMinutes(), false,job.getDisablementLocation().getTimeZone());
                }

                if (dateTime != null && dateTime.contains("am")) {
                    dateTime = dateTime.replace("am", "AM");
                }
                if (dateTime != null && dateTime.contains("pm")) {
                    dateTime = dateTime.replace("pm", "PM");
                }
                itemViewHolder.mTextEta.setText(dateTime);
            }else if (!TextUtils.isEmpty(job.getAcceptedDateTime())) {
                String dateTime;
                if (job.getEtaExtensionCount() == null || job.getEtaExtensionCount() == 0) {
                    dateTime = DateTimeUtils.getEtaDateTime(job.getAcceptedDateTime(), job.getQuotedEta(), true,job.getDisablementLocation().getTimeZone());
                } else {
                    dateTime = DateTimeUtils.getEtaDateTime(job.getAcceptedDateTime(), job.getTotalEtaInMinutes(), true,job.getDisablementLocation().getTimeZone());
                }

                if (dateTime != null && dateTime.contains("am")) {
                    dateTime = dateTime.replace("am", "AM");
                }
                if (dateTime != null && dateTime.contains("pm")) {
                    dateTime = dateTime.replace("pm", "PM");
                }
                itemViewHolder.mTextEta.setText(dateTime);
            } else {
                itemViewHolder.mTextEta.setText(R.string.not_available);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//    }

    @Override
    public int getItemCount() {
        return jobList.size();
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_active_job_title)
        TextView mTextActiveJobTitle;
        @BindView(R.id.text_active_job_status)
        TextView mTextActiveJobStatus;
        @BindView(R.id.text_active_job_disablement_address)
        TextView mTextActiveJobDisablementAddress;
        @BindView(R.id.text_active_job_tow_to_address)
        TextView mTextActiveJobTowToAddress;
        @BindView(R.id.text_view_details)
        TextView mTextActiveJobViewDetails;
        @BindView(R.id.constraint_tow_to)
        ConstraintLayout mConstraintTowTo;
        @BindView(R.id.constraint_disablement)
        ConstraintLayout mConstraintDisablement;
        @BindView(R.id.text_vertical_line_disablement)
        TextView mTextVerticalLineDesablement;
        @BindView(R.id.text_vertical_line_tow)
        TextView mTextVerticalLineTow;
        @BindView(R.id.image_pin_disablement)
        ImageView mImagePinDisablement;
        @BindView(R.id.view_status_disablement_divider)
        View mViewStatusDisablementDivider;
        @BindView(R.id.text_eta)
        TextView mTextEta;
        @BindView(R.id.text_active_job_id)
        TextView mJobId;

        ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    static class FooterItemHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.card_view_active_job_duty)
        CardView mCardViewActiveJobHint;
        @BindView(R.id.card_view_job_history)
        CardView mCardViewJobHistory;
        @BindView(R.id.text_duty_title)
        TextView mTextDutyTitle;
        @BindView(R.id.text_duty_description)
        TextView mTextDutyDescription;
        @BindView(R.id.text_activejobs_ok)
        TextView mTextActivejobsOk;


        FooterItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
