package com.agero.ncc.adapter;


import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.model.TowDestinationEntity;
import com.agero.ncc.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TowDestinationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private ArrayList<TowDestinationEntity> mTowDestinationEntities;
    private View mLastSelected;

    private TowDestinationEntity mSelectedTowDestinationEntity;
    public TowDestinationAdapter(Context mContext, List<TowDestinationEntity> towDestinationEntities) {
        this.mContext = mContext;
        this.mTowDestinationEntities = (ArrayList<TowDestinationEntity>) towDestinationEntities;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.tow_destination_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
        TowDestinationEntity towDestinationEntity = mTowDestinationEntities.get(position);

        itemViewHolder.mTextBusinessName.setText(towDestinationEntity.getVendorName());
        itemViewHolder.mTextAddress.setText(Utils.toCamelCase(towDestinationEntity.getAddress() + ", "
                + towDestinationEntity.getCity() + ", ") + towDestinationEntity.getState() + " " + towDestinationEntity.getPostalCode());
        itemViewHolder.mTextMiles.setText("" + towDestinationEntity.getDrivingDistance());
        itemViewHolder.itemView.setTag(towDestinationEntity);
    }

    @Override
    public int getItemCount() {
        return mTowDestinationEntities.size();
    }

    public TowDestinationEntity getSelectedItem(){
        return mSelectedTowDestinationEntity;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_business_name)
        TextView mTextBusinessName;
        @BindView(R.id.text_miles)
        TextView mTextMiles;
        @BindView(R.id.text_address)
        TextView mTextAddress;
        @BindView(R.id.image_tick_night_drop_off)
        ImageView mImageTickNightDropOff;
        @BindView(R.id.text_night_drop_off)
        TextView mtextNightDropOff;
        @BindView(R.id.image_tick_vehicle_storage)
        ImageView mImageTickVehicleStorage;
        @BindView(R.id.text_vehicle_storage)
        TextView mTextVehicleStorage;
        @BindView(R.id.constraint_tow_destination)
        ConstraintLayout mConstraintTowDestination;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mLastSelected != null) {
                        mLastSelected.setBackgroundResource(R.drawable.border_tap_to_sign);
                    }
                    v.setBackgroundResource(R.drawable.border_blue_selected);
                    mSelectedTowDestinationEntity = (TowDestinationEntity) v.getTag();
                    mLastSelected = v;

                    ((HomeActivity)mContext).saveEnable();
                }
            });

        }
    }
}
