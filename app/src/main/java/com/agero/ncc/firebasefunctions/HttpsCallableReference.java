package com.agero.ncc.firebasefunctions;

import android.support.annotation.Nullable;

import com.google.android.gms.tasks.Task;


public class HttpsCallableReference {
    private final FirebaseFunctions functionsClient;
    private final String name;

    HttpsCallableReference(FirebaseFunctions functionsClient, String name) {
        this.functionsClient = functionsClient;
        this.name = name;
    }

    public Task<HttpsCallableResult> call(@Nullable Object data) {
        return this.functionsClient.call(this.name, data);
    }

    public Task<HttpsCallableResult> call() {
        return this.functionsClient.call(this.name, (Object)null);
    }
}

