package com.agero.ncc.firebasefunctions;

import com.google.android.gms.tasks.Task;

public interface ContextProvider {
    Task<HttpsCallableContext> getContext();
}
