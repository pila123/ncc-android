package com.agero.ncc.views;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agero.ncc.R;
import com.agero.ncc.activities.HomeActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class AlertDetailBottomSheetDialog extends BottomSheetDialogFragment {

    HomeActivity mHomeActivity;
    AlertProfileDialogClickListener alertProfileDialogClickListener;

    public static AlertDetailBottomSheetDialog getInstance() {
        return new AlertDetailBottomSheetDialog();
    }

    public void setAlertDialogClickListener(AlertProfileDialogClickListener alertProfileDialogClickListener) {
        this.alertProfileDialogClickListener = alertProfileDialogClickListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_alert_bottom_sheet, container, false);
        ButterKnife.bind(this, view);
        mHomeActivity = (HomeActivity) getActivity();
        return view;
    }


    @OnClick({R.id.text_job_photo, R.id.text_call, R.id.text_delete_photo})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.text_job_photo:
                alertProfileDialogClickListener.onJobDetailClick();
                break;
            case R.id.text_call:
                alertProfileDialogClickListener.onContactDispatcherClick();
                break;
            case R.id.text_delete_photo:
                alertProfileDialogClickListener.onDeleteMessageClick();
                break;
        }
    }


    public interface AlertProfileDialogClickListener {

        public void onJobDetailClick();

        public void onContactDispatcherClick();

        public void onDeleteMessageClick();
    }
}
