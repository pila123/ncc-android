package com.agero.ncc.retrofit;

import com.agero.ncc.utils.DateTimeUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class RetrofitModule {
    String mBaseUrl;

    public RetrofitModule(String baseUrl) {
        this.mBaseUrl = baseUrl;
    }

    @Provides
    @Singleton
    Interceptor provideInterceptor() {
        Interceptor interceptorAPI = new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request request = null;
                try {
                    request = chain.request().newBuilder()
                            .addHeader("Content-Type", "application/json")
//                            .addHeader("ApiKey", "GaGmeSioEeGZ4N1XaRgzlfRbxyqeY1nr")
//                            .addHeader("X-REQ-LOCAL-TIME", DateTimeUtils.getReqCurrentDate())
//                            .addHeader("X-APP-VER", "1.0")
//                            .addHeader("X-APP-OS", "A")
                            .method(original.method(), original.body())
                            .build();
                } catch (Exception authFailureError) {
                    authFailureError.printStackTrace();
                }
                okhttp3.Response response = chain.proceed(request);

                return response;
            }
        };
        return interceptorAPI;
    }

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(Interceptor interceptor) {

        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();
        okHttpBuilder.interceptors().add(interceptor);
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        okHttpBuilder.interceptors().add(logging);

        okHttpBuilder.connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS);

        OkHttpClient client = okHttpBuilder.build();
        return client;
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
        Retrofit retrofit =
                new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create(gson))
                        .baseUrl(mBaseUrl)
                        .client(okHttpClient)
                        .build();
        return retrofit;
    }
}


