package com.agero.ncc.dispatcher.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.adapter.DispatcherJobsAdapter;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.firestore.FirestoreJobData;
import com.agero.ncc.fragments.AddJobsDetailsFragment;
import com.agero.ncc.fragments.BaseFragment;
import com.agero.ncc.model.Equipment;
import com.agero.ncc.model.JobDetail;
import com.agero.ncc.model.Status;
import com.agero.ncc.services.SparkLocationUpdateService;
import com.agero.ncc.utils.DispatcherJobDetailsComparator;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.agero.ncc.views.RecyclerItemClickListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.concurrent.Callable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ca.barrenechea.widget.recyclerview.decoration.DividerDecoration;
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderDecoration;
import durdinapps.rxfirebase2.RxFirebaseDatabase;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class ActiveJobsFragment extends BaseFragment implements HomeActivity.OnDutyToolbarListener {

    HomeActivity mHomeActivity;
    boolean isDenied;
    @BindView(R.id.constarint_settings)
    ConstraintLayout mConstarintSettings;
    @BindView(R.id.recycler_joblist)
    RecyclerView mRecyclerJoblist;

    @BindView(R.id.cons_activejobs)
    ConstraintLayout mConsActivejobs;
    @BindView(R.id.constraint_active_jobs)
    ConstraintLayout mConstraintActiveJobs;
    Unbinder unbinder;
    DispatcherJobsAdapter jobListAdapter;
    ArrayList<JobDetail> jobList;
    @BindView(R.id.text_lable_settings)
    TextView textLableSettings;
    @BindView(R.id.text_settings)
    TextView textSettings;
    @BindView(R.id.fab_add)
    FloatingActionButton mfabAdd;
    String mDispatchNumber;
    String selectedDispatchNumber;
    boolean isFromChat;
    int selectedJobPosition;
    @BindView(R.id.text_activejobs_ok)
    TextView mTextActivejobsOk;
    @BindView(R.id.card_view_active_job_duty)
    CardView mCardViewActiveJobHint;
    @BindView(R.id.text_activejobs_message)
    TextView mTextActivejobsMessage;
    View viewActive;
    UserError mUserError;
    private long oldRetryTime = 0;
    private DatabaseReference myRef, mDriverRef;
    private StickyHeaderDecoration mStickyHeaderDecoration;

    private int[] mJobTypeCountList = new int[3];
    private int serviceUnFinished = 0;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    ValueEventListener valueEventUserListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if(isAdded()) {
                hideProgress();
                loadUserData(dataSnapshot);
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            hideProgress();
            if (isAdded()) {
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("Dispatcher Active Jobs User Equipment Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    getUserDataFromFirebase();
                    oldRetryTime = System.currentTimeMillis();
                }
            }
        }
    };
    private ValueEventListener datavalueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {

//            if (!mHomeActivity.isOffDuty()) {
            for (int i = 0; i < mJobTypeCountList.length; i++) {
                mJobTypeCountList[i] = 0;
            }
            jobList.clear();
            int jobCount = 0;
            String userId = mPrefs.getString(NccConstants.SIGNIN_USER_ID, "");
            if (dataSnapshot.hasChildren()) {
                Iterable<DataSnapshot> childList = dataSnapshot.getChildren();
                String logEventText = "";
//                SparkLocationUpdateService.clearList();
                for (DataSnapshot data : childList) {
                    try {
                        JobDetail job = data.getValue(JobDetail.class);

                        if(NccConstants.IS_GEOFENCE_ENABLED) {
                            SparkLocationUpdateService.checkJobToAddOrRemove(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""), job);
                        }

                        if (job != null && job.getPartnerFacility() != null && !TextUtils.isEmpty(job.getPartnerFacility().getPhoneNumber())) {
                            mEditor.putString(NccConstants.DISPATCHER_NUMBER, job.getPartnerFacility().getPhoneNumber()).commit();
                        }
                        if (job != null && userId.equalsIgnoreCase(job.getDispatchAssignedToId())) {
                            jobCount++;
                        }
                        Gson gson = new Gson();
                        Timber.d(gson.toJson(job));
                        if (job != null && !TextUtils.isEmpty(job.getDispatchId())) {
                            addJobToList(job);
                        }
                        if (!logEventText.isEmpty()) {
                            logEventText += ", ";
                        }
                        logEventText += job.getDispatchId();
                    } catch (Exception e) {
                        mHomeActivity.mintLogException(e);
                        e.printStackTrace();
                    }
                }
                HashMap<String, String> extraDatas = new HashMap<>();
                extraDatas.put("jobs", logEventText);
                mHomeActivity.mintlogEventExtraData("Job List", extraDatas);
            }
            mEditor.putInt("jobsize", jobCount).commit();
            if(isAdded()) {
                populateAdapter();
            }
//            } else {
//                setOffDuty();
//            }
            hideProgress();

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            hideProgress();
            if (isAdded()) {
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("Dispatcher Active Jobs List Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    Timber.d("getDataFromFirebase called from onCancelled");
                    getDataFromFirebase();
                    oldRetryTime = System.currentTimeMillis();
                }
                Timber.e("Database Error" + databaseError.getMessage());
            }
        }
    };

    public ActiveJobsFragment() {
        // Intentionally empty
    }

    public static ActiveJobsFragment newInstance() {
        ActiveJobsFragment fragment = new ActiveJobsFragment();
        fragment.selectedDispatchNumber = "";
        fragment.selectedJobPosition = 0;
        return fragment;
    }

    public static ActiveJobsFragment newInstance(String dispatchNumber, boolean isFromChat) {
        ActiveJobsFragment fragment = new ActiveJobsFragment();
        Bundle bundle = new Bundle();
        bundle.putString(NccConstants.JOB_ID, dispatchNumber);
        bundle.putBoolean(NccConstants.IS_FROM_CHAT, isFromChat);
        fragment.setArguments(bundle);
        //fragment.selectedDispatchNumber = dispatchNumber;
        //fragment.isFromChat = isFromChat;
        //fragment.selectedJobPosition = 0;
        return fragment;
    }

    private void loadUserData(DataSnapshot dataSnapshot) {
        if (dataSnapshot != null) {
            try {
                Equipment equipment = dataSnapshot.getValue(Equipment.class);
                if (equipment != null) {
                    String json = new Gson().toJson(equipment);
                    HashMap<String, String> extraDatas = new HashMap<>();
                    extraDatas.put("json", json);
                    mHomeActivity.mintlogEventExtraData("Active Jobs Equipment Json", extraDatas);
                    mEditor.putString(NccConstants.EQUIPMENT_PREF_KEY, json).commit();
                    mEditor.putString(NccConstants.EQUIPMENT_ID, equipment.getEquipmentId()).commit();
                } else {
                    mEditor.putString(NccConstants.EQUIPMENT_PREF_KEY, new Gson().toJson("")).commit();
                    mEditor.putString(NccConstants.EQUIPMENT_ID, "-1").commit();
                }
            } catch (Exception e) {
                mHomeActivity.mintLogException(e);
                e.printStackTrace();
            }
        }
    }

    private void getUserDataFromFirebase() {
        showProgress();
        TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
            @Override
            public void onRefreshSuccess() {
                if (isAdded()) {
                    Disposable disposable = Observable.fromCallable((Callable<Boolean>) () -> {
                        try {
                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            mDriverRef = database.getReference("Users/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "") + "/"
                                    + mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
                            mDriverRef.keepSynced(true);
                            mDriverRef.child("Equipment").addValueEventListener(valueEventUserListener);
                            setStausChangeEventListener();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        return false;
                    }).subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe((result) -> {},Throwable::printStackTrace);

                    compositeDisposable.add(disposable);
                }
            }

            @Override
            public void onRefreshFailure() {
                if (isAdded()) {
                    mHomeActivity.tokenRefreshFailed();
                }
            }
        });
    }

    private void setStausChangeEventListener() {

        RxFirebaseDatabase.observeValueEvent(mDriverRef.child("onDuty"), Boolean.class)
                .subscribe(taskSnapshot -> {
                    if (taskSnapshot != null) {

                        if (!taskSnapshot) {
                            mHomeActivity.setDuty(getString(R.string.title_offduty));
//                    setOffDuty();
                            HashMap<String, String> extraDatas = new HashMap<>();
                            extraDatas.put("json", getString(R.string.title_offduty));
                            mHomeActivity.mintlogEventExtraData("Dispatcher Active Jobs Duty", extraDatas);

                            SparkLocationUpdateService.onDutyFlag = false;
                            mHomeActivity.stopSparkLocationService();

                        } else {
                            HashMap<String, String> extraDatas = new HashMap<>();
                            extraDatas.put("json", getString(R.string.title_onduty));
                            mHomeActivity.mintlogEventExtraData("Dispatcher Active Jobs Duty", extraDatas);
                            mHomeActivity.setDuty(getString(R.string.title_onduty));

                            SparkLocationUpdateService.onDutyFlag = true;
                            mHomeActivity.startSparkLocationService();
                        }
                        setOnDuty();
                    }
                }, throwable -> {
                    if (isAdded()) {
                        mHomeActivity.mintlogEvent("Dispatcher Active Jobs Duty Change Firebase Database Error - " + throwable.getLocalizedMessage());
                        if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                            setStausChangeEventListener();
                            oldRetryTime = System.currentTimeMillis();
                        }
                    }
                });
    }

    @Override
    public void showProgress() {
        if (serviceUnFinished <= 0) {
            super.showProgress();
        }
        serviceUnFinished++;
    }

    @Override
    public void hideProgress() {
        serviceUnFinished--;
        if (serviceUnFinished <= 0) {
            super.hideProgress();
        }
    }

    private void populateAdapter() {
        mHomeActivity.setBadgeCount(0, mJobTypeCountList[0] + mJobTypeCountList[1]);
        addHeaderSection();
        Collections.sort(jobList,new DispatcherJobDetailsComparator());
        setAdapterAndDecor(mRecyclerJoblist);
    }

    private void addJobToList(JobDetail job) {
        if (job != null && job.getCurrentStatus() != null && job.getServiceTypeCode() != null) {
            if (job.getCurrentStatus()
                    .getStatusCode()
                    .equalsIgnoreCase(NccConstants.JOB_STATUS_DECLINED)) {
                return;
            } else if ((job.getCurrentStatus()
                    .getStatusCode()
                    .equalsIgnoreCase(NccConstants.JOB_STATUS_OFFERED)) || (job.getCurrentStatus()
                    .getStatusCode()
                    .equalsIgnoreCase(NccConstants.JOB_STATUS_ACCEPTED)) || (job.getCurrentStatus()
                    .getStatusCode()
                    .equalsIgnoreCase(NccConstants.JOB_STATUS_AWARDED))) {
                mJobTypeCountList[0]++;
            } else if (job.getCurrentStatus()
                    .getStatusCode()
                    .equalsIgnoreCase(NccConstants.JOB_STATUS_UNASSIGNED)) {
                mJobTypeCountList[1]++;
            } else {
                mJobTypeCountList[2]++;
            }
            jobList.add(job);
        }

    }

    private void addHeaderSection() {
        int extraAdded = 0;
        if (mJobTypeCountList[0] == 0) {
            JobDetail jobDetail = new JobDetail();
            Status currentStatus = new Status();
            currentStatus.setStatusCode(NccConstants.JOB_STATUS_OFFERED);
            jobDetail.setCurrentStatus(currentStatus);
            jobList.add(0, jobDetail);
            extraAdded++;
        }
        if (mJobTypeCountList[1] == 0) {
            JobDetail jobDetail = new JobDetail();
            Status currentStatus = new Status();
            currentStatus.setStatusCode(NccConstants.JOB_STATUS_UNASSIGNED);
            jobDetail.setCurrentStatus(currentStatus);

            jobList.add(mJobTypeCountList[0] + extraAdded, jobDetail);

            extraAdded++;
        }
        if (mJobTypeCountList[2] == 0) {
            JobDetail jobDetail = new JobDetail();
            Status currentStatus = new Status();
            currentStatus.setStatusCode(NccConstants.JOB_STATUS_ASSIGNED);
            jobDetail.setCurrentStatus(currentStatus);
            jobList.add(mJobTypeCountList[0] + mJobTypeCountList[1] + extraAdded, jobDetail);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (isPermissionDenied()) {
            mConstarintSettings.setVisibility(View.VISIBLE);
        } else {
            mConstarintSettings.setVisibility(View.GONE);
        }

        final DividerDecoration divider = new DividerDecoration.Builder(getActivity()).setHeight(
                R.dimen.default_divider_height)
                .setPadding(R.dimen.default_divider_padding)
                .setColorResource(R.color.ncc_heading_color)
                .build();

        mRecyclerJoblist.setHasFixedSize(true);
        mRecyclerJoblist.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        mRecyclerJoblist.addItemDecoration(divider);
        setAdapterAndDecor(mRecyclerJoblist);

    }


    private void setAdapterAndDecor(RecyclerView recyclerJoblist) {
        if(isAdded()) {
            if (jobList != null && jobList.size() > 0) {
                if (selectedDispatchNumber == null || TextUtils.isEmpty(selectedDispatchNumber)) {
                    for (int i = 0; i < jobList.size(); i++) {
                        if (jobList.get(i).getDispatchId() != null) {
                            selectedDispatchNumber = jobList.get(i).getDispatchId();
                            selectedJobPosition = i;
                            break;
                        }
                    }
                }

                if (!TextUtils.isEmpty(selectedDispatchNumber)) {
                    if (getResources().getBoolean(R.bool.isTablet)) {
                        FirestoreJobData.getInStance().setFirestore(false);
                        mHomeActivity.push(JobDetailsFragment.newInstance(selectedDispatchNumber, false));
                    }
                }
            } else {
                mHomeActivity.hideRightFrame();
                mHomeActivity.showToolbar(getString(R.string.toolbar_text_duty));
            }

            jobListAdapter = new DispatcherJobsAdapter(getActivity(), jobList, mJobTypeCountList,
                    mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""), selectedJobPosition);

            try {
                if (mStickyHeaderDecoration != null) {
                    recyclerJoblist.removeItemDecoration(mStickyHeaderDecoration);
                }
                mStickyHeaderDecoration = new StickyHeaderDecoration(jobListAdapter);
                recyclerJoblist.addItemDecoration(mStickyHeaderDecoration, 0);
            } catch (Exception e) {
                e.printStackTrace();
            }

            recyclerJoblist.setAdapter(jobListAdapter);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        final View view = inflater.inflate(R.layout.fragment_activejobs, fragment_content, true);
        ButterKnife.bind(this, view);
        NCCApplication.getContext().getComponent().inject(this);
        mEditor = mPrefs.edit();
        NCCApplication.getContext().getComponent().inject(this);
        mHomeActivity = (HomeActivity) getActivity();
        mUserError = new UserError();
        mHomeActivity.showBottomBar();
        mHomeActivity.setOnDutyToolbarListener(this);
        mTextActivejobsMessage.setVisibility(View.GONE);
        mHomeActivity.showToolbar(getString(R.string.toolbar_text_duty));

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        mRecyclerJoblist.setLayoutManager(linearLayoutManager);
        mRecyclerJoblist.removeItemDecoration(
                new DividerItemDecoration(getActivity(), linearLayoutManager.getOrientation()));
        jobList = new ArrayList<>();

        mRecyclerJoblist.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        if(isAdded() && position != -1 && jobList.size() > position) {
                            mDispatchNumber = jobList.get(position).getDispatchId();
                            selectedDispatchNumber = mDispatchNumber;
                            selectedJobPosition = position;
                            FirestoreJobData.getInStance().setFirestore(false);
                            if (getResources().getBoolean(R.bool.isTablet)) {
                                mHomeActivity.push(JobDetailsFragment.newInstance(mDispatchNumber, false));
                            } else {
                                mHomeActivity.push(JobDetailsFragment.newInstance(mDispatchNumber, false),
                                        getString(R.string.title_assigned));
                            }
                        }
                    }
                }));

        mHomeActivity.setBGClickableFalse();
        mHomeActivity.registerFCM();
        setOffDuty();
        if (Utils.isNetworkAvailable()) {

            if(getArguments()!=null){
                selectedDispatchNumber = getArguments().getString(NccConstants.JOB_ID);
                isFromChat = getArguments().getBoolean(NccConstants.IS_FROM_CHAT);
            }

            if (!getResources().getBoolean(R.bool.isTablet) && !TextUtils.isEmpty(selectedDispatchNumber) && isFromChat) {
                FirestoreJobData.getInStance().setFirestore(false);
                mHomeActivity.push(JobDetailsFragment.newInstance(selectedDispatchNumber, false), getString(R.string.title_jobdetail));
                getArguments().remove(NccConstants.JOB_ID);
                getArguments().remove(NccConstants.IS_FROM_CHAT);
                isFromChat = false;
            }else{
                getUserDataFromFirebase();
                getUserStatusFromFirebase();
            }

        } else {
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
        return superView;
    }

    private void getUserStatusFromFirebase() {

    }

    private void getDataFromFirebase() {
        if (Utils.isNetworkAvailable()) {
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            if (!mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "").isEmpty()) {
                showProgress();
                TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                    @Override
                    public void onRefreshSuccess() {
                        if (isAdded()) {
                            myRef = database.getReference(
                                    "ActiveJobs/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "") + "/");
                            myRef.keepSynced(true);
                            myRef.addValueEventListener(datavalueEventListener);
                        }
                    }

                    @Override
                    public void onRefreshFailure() {
                        if (isAdded()) {
                            mHomeActivity.tokenRefreshFailed();
                        }
                    }
                });
            }
        } else {
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }

    @Override
    public void onDutyCall() {
//        setOnDuty();
        mHomeActivity.getLocationUpdates();
    }

    private void setOnDuty() {
        removeValueEventListners();
        getDataFromFirebase();

//        SparkLocationUpdateService.onDutyFlag = true;
//        mHomeActivity.startSparkLocationService();
    }

    @Override
    public void onTextDuty() {
        mHomeActivity.setBGClickableFalse();
    }

    @Override
    public void offDutyCall() {
//        setOffDuty();
        mHomeActivity.stopLocationUpdates();
    }

    private void setOffDuty() {
        removeValueEventListners();
        for (int i = 0; i < mJobTypeCountList.length; i++) {
            mJobTypeCountList[i] = 0;
        }
        if (jobList == null) {
            jobList = new ArrayList<>();
        }
        jobList.clear();
        if(isAdded()) {
            populateAdapter();
        }
    }

    public boolean isPermissionDenied() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return ContextCompat.checkSelfPermission(mHomeActivity.getBaseContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED
                    || !Utils.isGpsEnabled(getActivity());
        } else {
            return false;
        }
    }

    @OnClick({R.id.text_settings, R.id.fab_add})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.text_settings:
                Utils.gotoSettings(getActivity());
                break;
            case R.id.fab_add:
                mHomeActivity.push(AddJobsDetailsFragment.newInstance(true),
                        getString(R.string.title_add_job));
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        removeValueEventListners();

    }

    private void removeValueEventListners() {
        try{
        if (jobListAdapter != null) {
            jobListAdapter.removeValueEventListners();
            if (jobListAdapter.mDisposable != null) {
                jobListAdapter.mDisposable.dispose();
            }
        }
        if (myRef != null) {
            myRef.removeEventListener(datavalueEventListener);
        }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        removeValueEventListners();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        removeValueEventListners();
        if(compositeDisposable != null ){
            compositeDisposable.dispose();
        }
    }

}


