package com.agero.ncc.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.ChatActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.adapter.ChatNewConversationAdapter;
import com.agero.ncc.model.JobDetail;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UiUtils;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.agero.ncc.views.RecyclerItemClickListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.agero.ncc.model.Profile;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.chatsdk.core.dao.Thread;
import co.chatsdk.core.dao.User;
import co.chatsdk.core.interfaces.ThreadType;
import co.chatsdk.core.session.NM;
import co.chatsdk.core.session.StorageManager;
import co.chatsdk.firebase.wrappers.UserWrapper;
import co.chatsdk.ui.manager.InterfaceManager;
import co.chatsdk.ui.utils.ToastHelper;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class ChatNewConversationFragment extends BaseFragment {
    HomeActivity mHomeActivity;
    @BindView(R.id.text_chat_to)
    TextView mTextChatTo;
    @BindView(R.id.edit_chat_to)
    EditText mEditChatTo;
    @BindView(R.id.view_border_to)
    View mViewBorderTo;
    @BindView(R.id.constarint_to)
    ConstraintLayout constarintTo;
    @BindView(R.id.recycler_contact_details)
    RecyclerView mRecyclerContactDetails;
    @BindView(R.id.edit_write_message)
    EditText mEditWriteMessage;
    @BindView(R.id.text_send)
    TextView mTextSend;
    @BindView(R.id.view_border_message)
    View mViewBorderMessage;
    ChatNewConversationAdapter mChatNewConversationAdapter;
    ArrayList<Profile> userList;
    ArrayList<JobDetail> jobList;
    ArrayList<Profile> filteredList = new ArrayList<>();
    private String mThreadName;
    private List<User> mUsersList = new ArrayList<>();
    private boolean mIsJobChat;
    Profile selectedUser;
    public boolean isChatStarted = false;
    UserError mUserError;
    private User mUserProfile;
    private DatabaseReference myRef;
    private ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            try {
                myRef.removeEventListener(valueEventListener);
                jobList.clear();
                if (dataSnapshot.hasChildren()) {
                    Iterable<DataSnapshot> childList = dataSnapshot.getChildren();
                    for (DataSnapshot data : childList) {
                        try {
                            JobDetail job = data.getValue(JobDetail.class);
                            if (job != null) {
                                jobList.add(job);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            mChatNewConversationAdapter = new ChatNewConversationAdapter(getActivity(), userList, jobList);
            mRecyclerContactDetails.setAdapter(mChatNewConversationAdapter);
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    public ChatNewConversationFragment() {

    }

    public static ChatNewConversationFragment newInstance(ArrayList<Profile> mNewUserNamesArray, int threadType, String threadName) {
        ChatNewConversationFragment fragment = new ChatNewConversationFragment();
        Bundle bundle = new Bundle();
        bundle.putString(NccConstants.CHAT_USERS_LIST, new Gson().toJson(mNewUserNamesArray));
        bundle.putInt(NccConstants.CHAT_THREAD_TYPE, threadType);
        bundle.putString(NccConstants.CHAT_THREAD_NAME, threadName);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static ChatNewConversationFragment newInstance(ArrayList<Profile> mNewUserNamesArray, String threadName) {
        ChatNewConversationFragment fragment = new ChatNewConversationFragment();
        Bundle bundle = new Bundle();
        bundle.putString(NccConstants.CHAT_USERS_LIST, new Gson().toJson(mNewUserNamesArray));
        bundle.putString(NccConstants.CHAT_THREAD_NAME, threadName);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_chat_new_conversation, container, false);
        ButterKnife.bind(this, v);
        isChatStarted = false;
        mHomeActivity = (HomeActivity) getActivity();
        mHomeActivity.showToolbar(getString(R.string.new_conversation));
        mHomeActivity.hideBottomBar();
        mHomeActivity.hideSaveButton();
        mUserError = new UserError();
        jobList = new ArrayList<>();

        if (getArguments() != null) {
            mIsJobChat = getArguments().getInt(NccConstants.CHAT_THREAD_TYPE, ThreadType.Private1to1) == ThreadType.PrivateGroup;
            mUserProfile = NM.currentUser();
            Type type = new TypeToken<ArrayList<Profile>>() {
            }.getType();
            userList = new Gson().fromJson(getArguments().getString(NccConstants.CHAT_USERS_LIST, "[]"), type);
            if (!ChatActivity.isFromJobDetail) {
                mEditChatTo.requestFocus();
                mRecyclerContactDetails.setLayoutManager(new LinearLayoutManager(getActivity()));

                mRecyclerContactDetails.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        if (position >= jobList.size()) {
                            mIsJobChat = false;
                            position = position - jobList.size();

                            if (filteredList != null && filteredList.size() > 0) {
                                selectedUser = filteredList.get(position);//.getFirstName() + " "+userList.get(position).getLastName();
                            } else {
                                selectedUser = userList.get(position);//.getFirstName() + " "+userList.get(position).getLastName();
                            }

                            Thread presentThread = getIndividualPresentThread(selectedUser.getUserId());
                            if (presentThread == null) {
                                mRecyclerContactDetails.setVisibility(View.GONE);
                                mViewBorderMessage.setVisibility(View.VISIBLE);
                                mEditWriteMessage.setVisibility(View.VISIBLE);
                                mTextSend.setVisibility(View.VISIBLE);
                                String userName = "";
                                if (!TextUtils.isEmpty(selectedUser.getFirstName())) {
                                    userName = selectedUser.getFirstName().trim();
                                }
                                if (!TextUtils.isEmpty(selectedUser.getLastName())) {
                                    if (userName.length() > 0) {
                                        userName += " ";
                                    }
                                    userName += selectedUser.getLastName().trim();
                                }

                                mEditChatTo.setText(userName);
                                mEditChatTo.setSelection(mEditChatTo.getText().length());
                            } else {
                                startChat(presentThread, selectedUser.getUserId(), selectedUser, "");
                            }
                        } else {
                            mThreadName = getString(R.string.title_job_id) + UiUtils.getJobIdDisplayFormat(jobList.get(position).getDispatchId());
                            Thread thread = getPresentThread(mThreadName);
                            if (thread == null) {
                                mUsersList.clear();
                                mIsJobChat = true;
                                String userId = mPrefs.getString(NccConstants.SIGNIN_USER_ID, "");

                                for (Profile userProfile :
                                        userList) {
                                    if (mHomeActivity.isUserDispatcher(userProfile.getRoles()) || userId.equalsIgnoreCase(userProfile.getUserId())) {
                                        mUsersList.add(createUserForProfile(userProfile.getUserId(), userProfile));
                                    }
                                }
                                initJobChatUsers();
                            } else {
                                createPrivateGroup(thread);
                            }
                        }

                    }
                }));
                mEditChatTo.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        filter(s.toString());
                    }
                });
            } else {
                mThreadName = getArguments().getString(NccConstants.CHAT_THREAD_NAME);
                mIsJobChat = true;
                mUsersList.clear();
                for (Profile userProfile :
                        userList) {
                    mUsersList.add(createUserForProfile(userProfile.getUserId(), userProfile));
                }
                initJobChatUsers();

            }
            mEditWriteMessage.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (mEditWriteMessage.getText().toString().isEmpty()) {
                        mTextSend.setTextColor(getResources().getColor(R.color.ncc_text_disabled_color));
                        mTextSend.setEnabled(false);
                    } else {
                        mTextSend.setTextColor(getResources().getColor(R.color.ncc_blue));
                        mTextSend.setEnabled(true);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }
        if (mHomeActivity.isLoggedInDriver() || mHomeActivity.isUserDispatcherAndDriver()) {
            getJobDataFromFirebase();
        } else {
            mChatNewConversationAdapter = new ChatNewConversationAdapter(getActivity(), userList, jobList);
            mRecyclerContactDetails.setAdapter(mChatNewConversationAdapter);
        }
        return v;
    }

    private void initJobChatUsers() {
        mRecyclerContactDetails.setVisibility(View.GONE);
        mViewBorderMessage.setVisibility(View.VISIBLE);
        mEditWriteMessage.setVisibility(View.VISIBLE);
        mTextSend.setVisibility(View.VISIBLE);
        mEditChatTo.setVisibility(View.GONE);
        constarintTo.setVisibility(View.GONE);
    }

    private void filter(String text) {
//        ArrayList<Profile> filterdNames = new ArrayList<>();
        filteredList.clear();
        //looping through existing elements
        for (Profile profile : userList) {

            String userName = "";
            if (!TextUtils.isEmpty(profile.getFirstName())) {
                userName = profile.getFirstName().trim();
            }
            if (!TextUtils.isEmpty(profile.getLastName())) {
                if (userName.length() > 0) {
                    userName += " ";
                }
                userName += profile.getLastName().trim();
            }

            //if the existing elements contains the search input
            if (userName.toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filteredList.add(profile);
            }
        }
//        filteredList = filterdNames.toArray(new String[filterdNames.size()]);

        //calling a method of the adapter class and passing the filtered list
        if (mChatNewConversationAdapter != null) {
            mChatNewConversationAdapter.filterList(filteredList, jobList);
        }
    }

    @OnClick({R.id.text_send})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.text_send:
                if (Utils.isNetworkAvailable()) {
                    if (!mIsJobChat) {
                        Thread presentThread = getIndividualPresentThread(selectedUser.getUserId());
                        startChat(presentThread, selectedUser.getUserId(), selectedUser, mEditWriteMessage.getText().toString().trim());
                    } else {
                        Thread presentThread = getPresentThread(mThreadName);
                        createPrivateGroup(presentThread);
                    }
                } else {
                    mUserError.title = "";
                    mUserError.message = getString(R.string.network_error_message);
                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                }
                break;
        }
    }

    private Thread getPresentThread(String threadName) {

        Thread presentThread = null;
        List<Thread> threadList = NM.thread().getThreads(ThreadType.Private, false);
        for (Thread thread :
                threadList) {
            if (threadName.equalsIgnoreCase(thread.getName())) {
                presentThread = thread;
                break;
            }
        }
        return presentThread;
    }

    private void createPrivateGroup(Thread presentThread) {
        ChatActivity.toUserName = mThreadName;
        if (presentThread == null) {
            ChatActivity.messageToSend = mEditWriteMessage.getText().toString().trim();
            showProgress();
            NM.thread().createThread(mThreadName, mUsersList)
                    .observeOn(AndroidSchedulers.mainThread())
                    .doFinally(() -> {
                        dismissProgressDialog();
                    })
                    .subscribe(thread -> {
                        hideProgress();
                        isChatStarted = true;
//                        ChatActivity.isFromJobDetail = true;
                        InterfaceManager.shared().a.startChatActivityForID(mHomeActivity, thread.getEntityID());
                    }, throwable -> {
                        ToastHelper.show(mHomeActivity, throwable.getLocalizedMessage());
                    });
        } else if (!userInThread(presentThread, mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""))) {
            if (mUserProfile == null) {
                Profile profile = createProfile();
                mUserProfile = createUserForProfile(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""), profile);
            }
            if (mUserProfile != null) {
                presentThread.addUser(mUserProfile);
                isChatStarted = true;
//                ChatActivity.isFromJobDetail = true;
                InterfaceManager.shared().a.startChatActivityForID(mHomeActivity, presentThread.getEntityID());
            }
        } else {
            ChatActivity.messageToSend = "";
            isChatStarted = true;
//            ChatActivity.isFromJobDetail = true;
            InterfaceManager.shared().a.startChatActivityForID(mHomeActivity, presentThread.getEntityID());
        }
    }

    private Thread getIndividualPresentThread(String toUserId) {
        Thread presentThread = null;
        List<Thread> threadList = NM.thread().getThreads(ThreadType.Private1to1, false);
        for (Thread thread :
                threadList) {
            for (User user :
                    thread.getUsers()) {
                if (user.getEntityID().equalsIgnoreCase(toUserId) && (null == thread.getName() || !thread.getName().startsWith(getString(R.string.title_job_id)))) {
                    presentThread = thread;
                    break;
                }
            }
        }
        return presentThread;
    }

    public void startChat(Thread presentThread, String toUserId, Profile profile, String message) {
        ChatActivity.toUserName = profile.getFirstName() + " " + profile.getLastName();
        ChatActivity.messageToSend = message;
        if (presentThread == null) {
            showProgress();
            TokenManager.getInstance().validateToken(getActivity(), new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        User user = createUserForProfile(toUserId, profile);
                        NM.thread().createThread("", user, NM.currentUser())
                                .observeOn(AndroidSchedulers.mainThread())
                                .doFinally(() -> {
                                    dismissProgressDialog();
                                })
                                .subscribe(thread -> {
                                    isChatStarted = true;
                                    hideProgress();
                                    InterfaceManager.shared().a.startChatActivityForID(mHomeActivity, thread.getEntityID());
                                }, throwable -> {
                                    ToastHelper.show(mHomeActivity, throwable.getLocalizedMessage());
                                });
                    }
                }

                @Override
                public void onRefreshFailure() {
                    ((HomeActivity) getActivity()).tokenRefreshFailed();
                }
            });
        } else {
            isChatStarted = true;
            InterfaceManager.shared().a.startChatActivityForID(mHomeActivity, presentThread.getEntityID());
        }
    }

    private User createUserForProfile(String toUserId, Profile profile) {
        User user = StorageManager.shared().createEntity(User.class);

        final UserWrapper userWrapper1 = UserWrapper.initWithModel(user);


        userWrapper1.getModel().setEntityID(toUserId);
        userWrapper1.getModel().setEmail(profile.getEmail());
        userWrapper1.getModel().setName(profile.getFirstName() + " " + profile.getLastName());
        userWrapper1.getModel().setPhoneNumber(profile.getMobilePhoneNumber());
        userWrapper1.getModel().setAuthenticationType(2);
        userWrapper1.getModel().setLastOnline(new Date());
        userWrapper1.getModel().setLastUpdated(new Date());


        userWrapper1.getModel().update();

        return userWrapper1.getModel();
    }

    private Profile createProfile() {

        Profile userProfile = new Profile();
        userProfile.setUserId(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
        userProfile.setEmail(mPrefs.getString(NccConstants.SIGNIN_EMAILADDRESS, ""));
        String[] name = mPrefs.getString(NccConstants.SIGNIN_USER_NAME, "").split(" ");
        userProfile.setFirstName(name[0]);
        if (name.length > 1) {
            userProfile.setLastName(name[1]);
        } else {
            userProfile.setLastName("");
        }
        userProfile.setMobilePhoneNumber("");
        return userProfile;
    }

    private boolean userInThread(Thread presentThread, String userId) {
        boolean isUserPresent = false;
        for (User user :
                presentThread.getUsers()) {
            if (userId.equalsIgnoreCase(user.getEntityID())) {
                isUserPresent = true;
            }
        }
        return isUserPresent;
    }

    private void getJobDataFromFirebase() {
        if (Utils.isNetworkAvailable()) {
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            if (!mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "").isEmpty()) {
                showProgress();
                TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                    @Override
                    public void onRefreshSuccess() {
                        if (isAdded()) {
                            myRef = database.getReference("ActiveJobs/").child(mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""));
                            myRef.orderByChild("dispatchAssignedToId").equalTo(mPrefs.getString(NccConstants.SIGNIN_USER_ID, "")).addValueEventListener(valueEventListener);
                        }
                    }

                    @Override
                    public void onRefreshFailure() {
                        mHomeActivity.tokenRefreshFailed();
                    }
                });
            }
        } else {
            mUserError.title = "";
            mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }

}
