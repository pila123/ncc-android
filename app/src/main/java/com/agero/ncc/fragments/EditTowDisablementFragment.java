package com.agero.ncc.fragments;


import android.content.DialogInterface;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.model.DisablementLocation;
import com.agero.ncc.model.DisablementLocationUpdateModel;
import com.agero.ncc.model.JobDetail;
import com.agero.ncc.model.Profile;
import com.agero.ncc.model.Status;
import com.agero.ncc.model.TowLocation;
import com.agero.ncc.model.TowLocationUpdateModel;
import com.agero.ncc.utils.DateTimeUtils;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.agero.ncc.firebasefunctions.FirebaseFunctions;
import com.agero.ncc.firebasefunctions.FirebaseFunctionsException;
import com.agero.ncc.firebasefunctions.HttpsCallableResult;
import com.google.gson.Gson;
import com.vicmikhailau.maskededittext.MaskedFormatter;
import com.vicmikhailau.maskededittext.MaskedWatcher;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.ganfra.materialspinner.MaterialSpinner;
import timber.log.Timber;

public class EditTowDisablementFragment extends BaseFragment implements HomeActivity.ToolbarSaveListener {
    HomeActivity mHomeActivity;
    @BindView(R.id.edit_address1)
    EditText mEditAddress;
    @BindView(R.id.edit_address2)
    EditText mEditAddress1;
    @BindView(R.id.edit_city)
    EditText mEditCity;
    @BindView(R.id.text_label_add_tow_state)
    TextView mTextLabelAddTowState;
    @BindView(R.id.spinner_tow_state)
    Spinner mSpinnerTowState;
    @BindView(R.id.view_tow_state)
    View mViewTowState;
    @BindView(R.id.edit_zip_code)
    EditText mEditZipCode;
    @BindView(R.id.text_input_zip_code)
    TextInputLayout mTextInputZipCode;
    boolean isFromDisablement;
    String mUserName;
    DisablementLocation disablementLocation;
    TowLocation towLocation;
    String mOrginalAddress, mChangedAddress, mTitle;
    Date date;
    SimpleDateFormat mSimpleFormater;
    private DatabaseReference myRef;
    private String mDispatchNumber;
    private UserError mUserError;
    private long oldRetryTime = 0;
    @BindView(R.id.spinner_type)
    MaterialSpinner mSpinnerType;
    @BindView(R.id.text_business_name)
    TextInputLayout mTextBusinessName;
    @BindView(R.id.text_contact_number)
    TextInputLayout mTextContactNumber;
    @BindView(R.id.check_night_dropoff)
    CheckBox mCheckNightDropOff;
    @BindView(R.id.check_vehicle_storage)
    CheckBox mCheckVehicleStorage;
    @BindView(R.id.check_customer_disablement)
    CheckBox mCheckCustomerDisablement;
    @BindView(R.id.text_night_drop_off_avail)
    TextView mTextNightDropOffAvailable;
    @BindView(R.id.text_vehicle_storage_avail)
    TextView mTextVehicleStorageAvailable;
    @BindView(R.id.text_customer_disablement)
    TextView mTextCustomerDisablement;
    @BindView(R.id.text_poi)
    TextInputLayout mTextPoi;
    @BindView(R.id.text_parking_lot)
    TextInputLayout mTextParkingLot;
    @BindView(R.id.include_parking_garage)
    View mIncludeParkingGarage;
    @BindView(R.id.edit_contact_number)
    EditText mEditContactNumber;
    MaskedFormatter formatter;


    ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (isAdded()) {
                try {
                    if (isFromDisablement) {
                        disablementLocation = dataSnapshot.getValue(DisablementLocation.class);
                        loadDisablementValues();
                    } else {
                        towLocation = dataSnapshot.getValue(TowLocation.class);
                        loadTowValues();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                hideProgress();
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            hideProgress();
            if (isAdded()) {
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("Edit Tow Disablement Address Screen Job Details Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    getDataFromFirebase();
                    oldRetryTime = System.currentTimeMillis();
                }
            }
        }
    };

    TextWatcher textwatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            enableSave();
        }
    };

    public EditTowDisablementFragment() {
    }

    public static EditTowDisablementFragment newInstance(boolean isDisablement, String mAddress, String mUsername) {
        EditTowDisablementFragment fragment = new EditTowDisablementFragment();
        Bundle args = new Bundle();
        args.putString(NccConstants.DISPATCHER_NUMBER, mAddress);
        args.putBoolean(NccConstants.IS_IT_FROM_DISABLEMENT, isDisablement);
        args.putString(NccConstants.USERNAME, mUsername);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_edit_tow_disablement, fragment_content, true);
        ButterKnife.bind(this, view);
        mHomeActivity = (HomeActivity) getActivity();
        mHomeActivity.setOnToolbarSaveListener(this);
        mEditAddress.addTextChangedListener(textwatcher);
        mEditAddress1.addTextChangedListener(textwatcher);
        mEditCity.addTextChangedListener(textwatcher);
        mEditZipCode.addTextChangedListener(textwatcher);
        mEditContactNumber.addTextChangedListener(textwatcher);
        date = new Date();
        mSimpleFormater = new SimpleDateFormat("E, MMM d, hh:mm a", Locale.US);
        mUserError = new UserError();
        if (getArguments() != null) {
            isFromDisablement = getArguments().getBoolean(NccConstants.IS_IT_FROM_DISABLEMENT);
            mDispatchNumber = getArguments().getString(NccConstants.DISPATCHER_NUMBER);
            mUserName = getArguments().getString(NccConstants.USERNAME);
        }

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        if (isFromDisablement) {
            myRef = database.getReference("ActiveJobs/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "") + "/" + mDispatchNumber + "/disablementLocation");
           // myRef.keepSynced(true);
        } else {
            myRef = database.getReference("ActiveJobs/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "") + "/" + mDispatchNumber + "/towLocation");
            //myRef.keepSynced(true);
        }


        if (Utils.isNetworkAvailable()) {
            getDataFromFirebase();
        }

        formatter = new MaskedFormatter("(###) ###-####");
        mEditContactNumber.addTextChangedListener(new MaskedWatcher(formatter, mEditContactNumber));

        return superView;
    }

    private void getDataFromFirebase() {
        showProgress();
        TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
            @Override
            public void onRefreshSuccess() {
                if (isAdded()) {
                    myRef.addValueEventListener(valueEventListener);
                }
            }

            @Override
            public void onRefreshFailure() {
                hideProgress();
                if(isAdded()) {
                    mHomeActivity.tokenRefreshFailed();
                }
            }
        });
    }


    private void loadDisablementValues() {

        mTextBusinessName.setVisibility(View.GONE);
        mTextContactNumber.setVisibility(View.GONE);
        mCheckNightDropOff.setVisibility(View.GONE);
        mCheckVehicleStorage.setVisibility(View.GONE);
        mTextNightDropOffAvailable.setVisibility(View.GONE);
        mTextVehicleStorageAvailable.setVisibility(View.GONE);
        mCheckCustomerDisablement.setVisibility(View.VISIBLE);
        mTextCustomerDisablement.setVisibility(View.VISIBLE);
        mTextPoi.setVisibility(View.VISIBLE);

        mHomeActivity.showToolbar(getString(R.string.title_edit_disablement));
        mHomeActivity.updateAnalyticsCurrentScreen(getResources().getString(R.string.label_edit_disablement));
        if (disablementLocation != null) {
            mEditAddress.setText(disablementLocation.getAddressLine1());
            mEditAddress1.setText(disablementLocation.getAddressLine2());
            mEditCity.setText(disablementLocation.getCity());
            mEditZipCode.setText(disablementLocation.getPostalCode());
            try {
                List<String> states = Arrays.asList(getResources().getStringArray(R.array.array_states));
                mSpinnerTowState.setSelection(states.indexOf(disablementLocation.getState()));
                setAdapter();
            } catch (Exception e) {
                e.printStackTrace();
            }
            mTitle = getString(R.string.lable_disablement_changed);
            if (!TextUtils.isEmpty(disablementLocation.getAddressLine2())) {
                mOrginalAddress = disablementLocation.getAddressLine1() + ", " + disablementLocation.getAddressLine2() + ", " + disablementLocation.getCity() + ", " + disablementLocation.getPostalCode();
            } else {
                mOrginalAddress = disablementLocation.getAddressLine1() + ", " + disablementLocation.getCity() + ", " + disablementLocation.getPostalCode();
            }

        }
    }

    private void loadTowValues() {
        mTextBusinessName.setVisibility(View.VISIBLE);
        mTextContactNumber.setVisibility(View.VISIBLE);
        mCheckNightDropOff.setVisibility(View.VISIBLE);
        mCheckVehicleStorage.setVisibility(View.VISIBLE);
        mTextNightDropOffAvailable.setVisibility(View.VISIBLE);
        mTextVehicleStorageAvailable.setVisibility(View.VISIBLE);
        mCheckCustomerDisablement.setVisibility(View.GONE);
        mTextCustomerDisablement.setVisibility(View.GONE);
        mTextPoi.setVisibility(View.GONE);

        mHomeActivity.showToolbar(getString(R.string.title_edit_tow));
        mHomeActivity.updateAnalyticsCurrentScreen(getResources().getString(R.string.label_edit_tow));
        if (towLocation != null) {
            mEditAddress.setText(towLocation.getAddressLine1());
            mEditAddress1.setText(towLocation.getAddressLine2());
            mEditCity.setText(towLocation.getCity());
            mEditZipCode.setText(towLocation.getPostalCode());
            try {
                List<String> states = Arrays.asList(getResources().getStringArray(R.array.array_states));
                mSpinnerTowState.setSelection(states.indexOf(towLocation.getState()));
            } catch (Exception e) {
                e.printStackTrace();
            }
            mTitle = getString(R.string.lable_tow_destination_changed);
            if (!TextUtils.isEmpty(towLocation.getAddressLine2())) {
                mOrginalAddress = towLocation.getAddressLine1() + ", " + towLocation.getAddressLine2() + ", " + towLocation.getCity() + ", " + towLocation.getPostalCode();
            } else {
                mOrginalAddress = towLocation.getAddressLine1() + ", " + towLocation.getCity() + ", " + towLocation.getPostalCode();
            }

        }
    }

    private void setAdapter() {
        mSpinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedType = mSpinnerType.getItemAtPosition(i).toString();
                String[] typeArray = getResources().getStringArray(R.array.array_type);
                if (selectedType.equals(typeArray[1])) {
                    mIncludeParkingGarage.setVisibility(View.GONE);
                    mTextParkingLot.setVisibility(View.VISIBLE);
                } else if (selectedType.equals(typeArray[2])) {
                    mIncludeParkingGarage.setVisibility(View.VISIBLE);
                    mTextParkingLot.setVisibility(View.GONE);
                } else {
                    mIncludeParkingGarage.setVisibility(View.GONE);
                    mTextParkingLot.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void enableSave() {
        if (isAdded()) {

            if (mEditAddress1.getText().toString().trim().length() > 0) {
                mChangedAddress = mEditAddress.getText().toString() + ", " + mEditAddress1.getText().toString() + ", " + mEditCity.getText().toString() + ", " + mEditZipCode.getText().toString();
            } else {
                mChangedAddress = mEditAddress.getText().toString() + ", " + mEditCity.getText().toString() + ", " + mEditZipCode.getText().toString();
            }


            if (mOrginalAddress != null && mChangedAddress != null && (mEditAddress.getText().toString().isEmpty() || mEditCity.getText().toString().trim().isEmpty() ||
                    mSpinnerTowState.getSelectedItemPosition() <= 0 || mEditZipCode.getText().toString().trim().isEmpty() || mOrginalAddress.equalsIgnoreCase(mChangedAddress))) {
                mHomeActivity.saveDisable();
            } else {
                mHomeActivity.saveEnable();
            }
        }
    }

    @Override
    public void onSave() {
        towLocationAlertDialog();
    }

    private void towLocationAlertDialog() {
        AlertDialogFragment alert = AlertDialogFragment.newDialog(Html.fromHtml(mTitle), Html.fromHtml("<font color='#999999'>" + "Address: " + mChangedAddress + ". Requested by " + mUserName + " " + getString(R.string.dot) + " " + mSimpleFormater.format(date) + "<br><br>" + "Original Address: " + mOrginalAddress + "</font>")
                , "", Html.fromHtml("<b>" + getString(R.string.ok) + "</b>"));

        alert.setListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                saveLocation();
                dialogInterface.dismiss();
            }
        });
        alert.show(getFragmentManager().beginTransaction(), EditTowDisablementFragment.class.getClass().getCanonicalName());
    }

    public void saveLocation() {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            LatLng latLng = getLocationFromAddress(mChangedAddress);
            if (latLng.latitude != 0.0 && latLng.longitude != 0.0) {
                TokenManager.getInstance().validateToken(((BaseActivity) getActivity()), new TokenManager.TokenReponseListener() {
                    @Override
                    public void onRefreshSuccess() {
                        if (isAdded()) {
                            if (isFromDisablement && disablementLocation != null) {

//                                disablementLocation.setStatus("REQUEST");
//                                String jsonString = new Gson().toJson(disablementLocation);

                                DisablementLocation newDisablementLocation = disablementLocation;
                                newDisablementLocation.setAddressLine1(mEditAddress.getText().toString().trim());
                                newDisablementLocation.setAddressLine2(mEditAddress1.getText().toString().trim());
                                newDisablementLocation.setCity(mEditCity.getText().toString().trim());
                                newDisablementLocation.setState(mSpinnerTowState.getSelectedItem().toString());
                                newDisablementLocation.setPostalCode(mEditZipCode.getText().toString().trim());


                                newDisablementLocation.setLatitude(latLng.latitude);
                                newDisablementLocation.setLongitude(latLng.longitude);

                                newDisablementLocation.setUserId(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
                                newDisablementLocation.setUserName(mPrefs.getString(NccConstants.SIGNIN_USER_NAME, ""));
                                newDisablementLocation.setStatusTime(DateTimeUtils.getUtcTimeFormat());
                                newDisablementLocation.setServerTimeUtc(DateTimeUtils.getUtcTimeFormat());
                                newDisablementLocation.setSource("FIREBASE");
                                newDisablementLocation.setSubSource(NccConstants.SUB_SOURCE);
                                newDisablementLocation.setDeviceId(Settings.Secure.getString(NCCApplication.getContext().getContentResolver(), Settings.Secure.ANDROID_ID));

                                HashMap<String, String> extraDatas = new HashMap<>();
                                extraDatas.put("disablement_location", new Gson().toJson(newDisablementLocation));
                                extraDatas.put("dispatch_id", mDispatchNumber);
                                ((HomeActivity) getActivity()).mintlogEventExtraData("Update Disablement Address", extraDatas);


                                updateDisablementLocationAPI(newDisablementLocation);


                            } else if (!isFromDisablement && towLocation != null) {

//                                String jsonString = new Gson().toJson(towLocation);

                                TowLocation newTowLocation = towLocation;

                                newTowLocation.setAddressLine1(mEditAddress.getText().toString().trim());
                                newTowLocation.setAddressLine2(mEditAddress1.getText().toString().trim());
                                newTowLocation.setCity(mEditCity.getText().toString().trim());
                                newTowLocation.setState(mSpinnerTowState.getSelectedItem().toString());
                                newTowLocation.setPostalCode(mEditZipCode.getText().toString().trim());

                                LatLng latLng = getLocationFromAddress(mChangedAddress);
                                newTowLocation.setLatitude(latLng.latitude);
                                newTowLocation.setLongitude(latLng.longitude);
                                newTowLocation.setUserId(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
                                newTowLocation.setUserName(mPrefs.getString(NccConstants.SIGNIN_USER_NAME, ""));
                                newTowLocation.setStatusTime(DateTimeUtils.getUtcTimeFormat());
                                newTowLocation.setServerTimeUtc(DateTimeUtils.getUtcTimeFormat());
                                newTowLocation.setSource("FIREBASE");
                                newTowLocation.setSubSource(NccConstants.SUB_SOURCE);
                                newTowLocation.setDeviceId(Settings.Secure.getString(NCCApplication.getContext().getContentResolver(), Settings.Secure.ANDROID_ID));

                                HashMap<String, String> extraDatas = new HashMap<>();
                                extraDatas.put("tow_location", new Gson().toJson(newTowLocation));
                                extraDatas.put("dispatch_id", mDispatchNumber);
                                ((HomeActivity) getActivity()).mintlogEventExtraData("Update Tow Address", extraDatas);

                                updateTowLocationAPI(newTowLocation);
                            } else {
                                hideProgress();
                                mUserError.title = getString(R.string.invalid_address_title);
                                mUserError.message = getString(R.string.invalid_address_message);
                                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                            }

                        }
                    }

                    @Override
                    public void onRefreshFailure() {
                        hideProgress();
                        if(isAdded()) {
                            ((BaseActivity) getActivity()).tokenRefreshFailed();
                        }
                    }
                });
            } else {
                hideProgress();
                mUserError.title = getString(R.string.invalid_address_title);
                mUserError.message = getString(R.string.invalid_address_message);
                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
            }
        } else {
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }

    private void updateDisablementLocationAPI(DisablementLocation newDisablementLocation) {
        DisablementLocationUpdateModel disablementLocationUpdateModel = new DisablementLocationUpdateModel(
                newDisablementLocation,
                mPrefs.getString(NccConstants.APIGEE_TOKEN, ""),
                DateTimeUtils.getUtcTimeFormat(),
                NccConstants.SUB_SOURCE,
                mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""),
                mPrefs.getString(NccConstants.SIGNIN_USER_ID, "")
        );
        FirebaseFunctions.getInstance()
                .getHttpsCallable(NccConstants.API_JOB_DL_UPDATE)
                .call(new Gson().toJson(disablementLocationUpdateModel))
                .addOnFailureListener(new OnFailureListener() {

                    @Override
                    public void onFailure(@NonNull Exception e) {
                        hideProgress();
                        Timber.d("failed");
//                                    e.printStackTrace();
                        UserError mUserError = new UserError();
                        if (e instanceof FirebaseFunctionsException) {
                            FirebaseFunctionsException ffe = (FirebaseFunctionsException) e;
                            FirebaseFunctionsException.Code code = ffe.getCode();
                            Object details = ffe.getDetails();
//                                        Toast.makeText(((BaseActivity) getActivity()), ffe.getMessage(), Toast.LENGTH_LONG).show();
                            ((HomeActivity) getActivity()).mintlogEvent("Disablement Location Update: " + " Firebase Functions Error - Error Message -" + ffe.getMessage());
                                mUserError.message = ffe.getMessage();
                                try {
                                    JSONObject errorObj = new JSONObject(ffe.getMessage());
                                    mUserError.title = errorObj.optString("title");
                                    mUserError.message = errorObj.optString("body");
                                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);

                                } catch (Exception parseException) {
                                    parseException.printStackTrace();
                                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                }
                                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);

                        } else if (e.getMessage() != null) {
                            if(getString(R.string.firebase_logout_error_msg).equalsIgnoreCase(e.getMessage())){
                                ((HomeActivity) getActivity()).tokenRefreshFailed();
                            }else {
                                mUserError.message = e.getMessage();
                                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                ((HomeActivity) getActivity()).mintlogEvent("Disablement Location Update: " + " Firebase Functions Error - Error Message -" + e.getMessage());
                            }
                        } else {
                            ((HomeActivity) getActivity()).mintlogEvent("Disablement Location Update Firebase Functions Error - Error Message -  failed");
                            mUserError.message = "failed";
                            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                        }
                    }
                })
                .addOnSuccessListener(new OnSuccessListener<HttpsCallableResult>() {
                    @Override
                    public void onSuccess(HttpsCallableResult httpsCallableResult) {
                        hideProgress();
                        mHomeActivity.popBackStackImmediate();
                    }
                });
    }

    private void updateTowLocationAPI(TowLocation newTowLocation) {
        TowLocationUpdateModel disablementLocationUpdateModel = new TowLocationUpdateModel(
                newTowLocation,
                mPrefs.getString(NccConstants.APIGEE_TOKEN, ""),
                DateTimeUtils.getUtcTimeFormat(),
                NccConstants.SUB_SOURCE,
                mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""),
                mPrefs.getString(NccConstants.SIGNIN_USER_ID, "")
        );
        FirebaseFunctions.getInstance()
                .getHttpsCallable(NccConstants.API_JOB_TL_UPDATE)
                .call(new Gson().toJson(disablementLocationUpdateModel))
                .addOnFailureListener(new OnFailureListener() {

                    @Override
                    public void onFailure(@NonNull Exception e) {
                        hideProgress();
                        Timber.d("failed");
//                                    e.printStackTrace();
                        UserError mUserError = new UserError();
                        if (e instanceof FirebaseFunctionsException) {
                            FirebaseFunctionsException ffe = (FirebaseFunctionsException) e;
                            FirebaseFunctionsException.Code code = ffe.getCode();
                            Object details = ffe.getDetails();
//                                        Toast.makeText(((BaseActivity) getActivity()), ffe.getMessage(), Toast.LENGTH_LONG).show();
                            ((HomeActivity) getActivity()).mintlogEvent("Disablement Location Update: " + " Firebase Functions Error - Error Message -" + ffe.getMessage());
                                mUserError.message = ffe.getMessage();
                                try {
                                    JSONObject errorObj = new JSONObject(ffe.getMessage());
                                    mUserError.title = errorObj.optString("title");
                                    mUserError.message = errorObj.optString("body");
                                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);

                                } catch (Exception parseException) {
                                    parseException.printStackTrace();
                                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                }
                                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);

                        } else if (e.getMessage() != null) {
                            if(getString(R.string.firebase_logout_error_msg).equalsIgnoreCase(e.getMessage())){
                                ((HomeActivity) getActivity()).tokenRefreshFailed();
                            }else {
                                mUserError.message = e.getMessage();
                                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                ((HomeActivity) getActivity()).mintlogEvent("Disablement Location Update: " + " Firebase Functions Error - Error Message -" + e.getMessage());
                            }
                        } else {
                            ((HomeActivity) getActivity()).mintlogEvent("Disablement Location Update Firebase Functions Error - Error Message -  failed");
                            mUserError.message = "failed";
                            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                        }
                    }
                })
                .addOnSuccessListener(new OnSuccessListener<HttpsCallableResult>() {
                    @Override
                    public void onSuccess(HttpsCallableResult httpsCallableResult) {
                        hideProgress();
                        mHomeActivity.popBackStackImmediate();
                    }
                });
    }

    public LatLng getLocationFromAddress(String strAddress) {

        Geocoder coder = new Geocoder(mHomeActivity);
        List<Address> address;
        LatLng p1 = null;

        try {
            address = coder.getFromLocationName(strAddress, 1);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng((double) (location.getLatitude() * 1E6),
                    (double) (location.getLongitude() * 1E6));

            return p1;
        } catch (Exception e) {
            p1 = new LatLng(0.0, 0.0);
        }
        return p1;
    }
}
