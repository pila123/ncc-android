package com.agero.ncc.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.agero.ncc.R;
import com.agero.ncc.activities.HomeActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddCompanyFragment extends BaseFragment implements HomeActivity.ToolbarSaveListener {
    @BindView(R.id.edit_add_company_verification_code)
    EditText mEditAddCompanyVerificationCode;
    private HomeActivity mHomeActivity;
    boolean inviteCode;

    public AddCompanyFragment() {
    }

    public static AddCompanyFragment newInstance() {
        AddCompanyFragment fragment = new AddCompanyFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_add_company, fragment_content, true);
        ButterKnife.bind(this, view);
        mHomeActivity = (HomeActivity) getActivity();
        mHomeActivity.setOnToolbarSaveListener(this);
        mHomeActivity.showToolbar(getString(R.string.title_add_company));
        return superView;
    }

    @Override
    public void onSave() {
//        if (inviteCode) {
//            companyDialog(getResources().getString(R.string.add_company_alert_message),
//                    getResources().getString(R.string.company_details),
//                    getResources().getString(R.string.label_continue),
//                    getResources().getString(R.string.cancel)
//                    );
//        }
//        else{
            companyDialog(getResources().getString(R.string.add_company_invalid_alert_title),
                    getResources().getString(R.string.add_company_invalid_alert_message),
                    "",
                    getResources().getString(R.string.add_company_invalid_try)
                    );
//        }
    }

    private void companyDialog(String title,String message,String pasitive, String negative) {
        AlertDialogFragment alert = AlertDialogFragment.newDialog(Html.fromHtml("<font color =\"#DE000000\"><b>" + title + "</b></font>"),
                Html.fromHtml("<font color =\"#999999\">" + message + "</font>")
                , pasitive, Html.fromHtml("<font><b>" + negative + "</b></font>"));
        alert.setListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alert.show(getFragmentManager().beginTransaction(), AddCompanyFragment.class.getClass().getCanonicalName());
    }
}
