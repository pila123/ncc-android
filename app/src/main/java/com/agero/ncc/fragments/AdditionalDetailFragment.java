package com.agero.ncc.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.model.JobDetail;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdditionalDetailFragment extends BaseFragment {

    HomeActivity mHomeActivity;
    @BindView(R.id.text_service_details_service)
    TextView mTextServiceDetailsService;
    @BindView(R.id.text_service_details_equipment)
    TextView mTextServiceDetailsEquipment;
    @BindView(R.id.text_service_details_reason)
    TextView mTextServiceDetailsReason;
    @BindView(R.id.text_vehicle_model)
    TextView mTextVehicleModel;
    @BindView(R.id.text_vehicle_color)
    TextView mTextVehicleColor;
    @BindView(R.id.text_vehicle_vin)
    TextView mTextVehicleVin;
    @BindView(R.id.text_vehicle_plate)
    TextView mTextVehiclePlate;
    @BindView(R.id.text_vehicle_type)
    TextView mTextVehicleType;
    @BindView(R.id.text_vehicle_drive_train)
    TextView mTextVehicleDriveTrain;
    @BindView(R.id.text_vehicle_fuel)
    TextView mTextVehicleFuel;
    @BindView(R.id.text_customer_details_name)
    TextView mTextCustomerDetailsName;
    @BindView(R.id.text_customer_details_coverage)
    TextView mTextCustomerDetailsCoverage;
    @BindView(R.id.text_customer_details_customer_pays)
    TextView mTextCustomerDetailsCustomerPays;
    @BindView(R.id.text_customer_details_payment_method)
    TextView mTextCustomerDetailsPaymentMethod;
    UserError mUserError;
    String mDispatchNumber;
    boolean isItFromHistory;
    private long oldRetryTime = 0;
    private DatabaseReference myRef;
    private ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (dataSnapshot.hasChildren() && isAdded()) {
                try {
                    JobDetail job = dataSnapshot.getValue(JobDetail.class);
                    HashMap<String, String> extraDatas = new HashMap<>();
                    extraDatas.put("json", new Gson().toJson(job));
                    mHomeActivity.mintlogEventExtraData("Additional Details Screen Job", extraDatas);
                    additionalDetails(job);
                } catch (Exception e) {
                    mHomeActivity.mintLogException(e);
                }
            }
            hideProgress();
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            hideProgress();
            if(isAdded()) {
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("Additional Detail Job Details Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    getDataFromFirebase();
                    oldRetryTime = System.currentTimeMillis();
                }
            }
        }
    };

    public AdditionalDetailFragment() {
        // Intentionally empty
    }

    public static AdditionalDetailFragment newInstance(String sectionNumber, boolean isHistory) {
        AdditionalDetailFragment fragment = new AdditionalDetailFragment();
        Bundle args = new Bundle();
        args.putString(NccConstants.DISPATCH_REQUEST_NUMBER, sectionNumber);
        args.putBoolean(NccConstants.IS_IT_FROM_HISTORY, isHistory);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_additional_detail, fragment_content, true);
        ButterKnife.bind(this, view);
        NCCApplication.getContext().getComponent().inject(this);
        mEditor = mPrefs.edit();
        mHomeActivity = (HomeActivity) getActivity();
        mHomeActivity.hideBottomBar();
        mUserError = new UserError();
        mHomeActivity.showToolbar(getString(R.string.title_additional_details));

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            mDispatchNumber = bundle.getString(NccConstants.DISPATCH_REQUEST_NUMBER);
            isItFromHistory = bundle.getBoolean(NccConstants.IS_IT_FROM_HISTORY);
        }
        getDataFromFirebase();
        return superView;
    }

    private void getDataFromFirebase() {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        FirebaseDatabase database = FirebaseDatabase.getInstance();

                        if (isItFromHistory) {
                            myRef = database.getReference("InActiveJobs/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "") + "/" + mDispatchNumber);
                            //myRef.keepSynced(true);
                        } else {
                            myRef = database.getReference("ActiveJobs/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "") + "/" + mDispatchNumber);
                            //myRef.keepSynced(true);
                        }


                        myRef.addValueEventListener(valueEventListener);

                    }
                }

                @Override
                public void onRefreshFailure() {
                    hideProgress();
                    if(isAdded()) {
                        mHomeActivity.tokenRefreshFailed();
                    }
                }
            });
        } else {
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }

    }

    private void additionalDetails(JobDetail job) {
        //Service
        try {
            mTextServiceDetailsService.setText(getString(R.string.additional_info_service_details_service) + " " + Utils.getDisplayServiceTypes(mHomeActivity,job.getServiceTypeCode()));
            mTextServiceDetailsEquipment.setText(getString(R.string.additional_info_service_details_equipemnt) + " " + job.getEquipmentList().get(0).getEquipmentTypeName());
            mTextServiceDetailsReason.setText(getString(R.string.additional_info_service_details_reason) + " " + job.getCallReason());
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Vehicle
        try {
            mTextVehicleModel.setText(getString(R.string.additional_info_vehicle_model) + " " + job.getVehicle().getYear()
                    + " " + job.getVehicle().getMake() + " " + job.getVehicle().getModel());
            mTextVehicleColor.setText(getString(R.string.additional_info_vehicle_color) + " " + job.getVehicle().getColor());
            mTextVehicleVin.setText(getString(R.string.additional_info_vehicle_VIN) + " " + job.getVehicle().getVin());
            mTextVehiclePlate.setText(getString(R.string.additional_info_vehicle_plate) + " " + job.getVehicle().getPlate());
            mTextVehicleType.setText(getString(R.string.additional_info_vehicle_type) + " " + job.getVehicle().getVehicleType());
            mTextVehicleDriveTrain.setText(getString(R.string.additional_info_vehicle_drive_train) + " " + job.getVehicle().getDriveTrainType());
            mTextVehicleFuel.setText(getString(R.string.additional_info_vehicle_fuel) + " " + job.getVehicle().getFuelType());
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Customer
        try {
            mTextCustomerDetailsName.setText(getString(R.string.additonal_info_customer_details_name) + " " + job.getCustomerName());
            mTextCustomerDetailsCoverage.setText(getString(R.string.additonal_info_customer_details_coverage) + " " + job.getCoverage());
            mTextCustomerDetailsCustomerPays.setText(getString(R.string.additonal_info_customer_details_customer_pays) + " " + job.getCustomerPays());
            mTextCustomerDetailsPaymentMethod.setText(getString(R.string.additonal_info_customer_details_payment_method) + " " + job.getCustomerPays());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(myRef != null) {
            myRef.removeEventListener(valueEventListener);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if(myRef != null) {
            myRef.removeEventListener(valueEventListener);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(myRef != null) {
            myRef.removeEventListener(valueEventListener);
        }
    }

}
