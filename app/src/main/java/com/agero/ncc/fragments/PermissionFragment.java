package com.agero.ncc.fragments;


import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agero.ncc.R;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.utils.NccConstants;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class PermissionFragment extends BaseFragment {

    HomeActivity mHomeActivity;
    String eventAction = "";
    String eventCategory = NccConstants.FirebaseEventCategory.PERMISSION_REQUEST;

    public PermissionFragment() {
        // Intentionally empty
    }

    public static PermissionFragment newInstance() {
        PermissionFragment fragment = new PermissionFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_permission, fragment_content, true);
        ButterKnife.bind(this, view);
        NCCApplication.getContext().getComponent().inject(this);
        mEditor = mPrefs.edit();
        mHomeActivity = (HomeActivity) getActivity();
        mHomeActivity.hideBottomBar();
        mHomeActivity.hideToolBar();
        mHomeActivity.hideDuty();
        return superView;
    }


    @OnClick({R.id.text_label_not_now, R.id.button_allow_location})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.text_label_not_now:
                mHomeActivity.startActiveJobs();
                eventAction = NccConstants.FirebaseEventAction.SKIP_PERMISSION;
                mHomeActivity.firebaseLogEvent(eventCategory,eventAction);
                break;
            case R.id.button_allow_location:
                if (!Dexter.isRequestOngoing()) {
                    Dexter.checkPermission(permissionListener, Manifest.permission.ACCESS_FINE_LOCATION);
                }
                eventAction = NccConstants.FirebaseEventAction.ALLOW_PERMISSION;
                mHomeActivity.firebaseLogEvent(eventCategory,eventAction);
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mHomeActivity.hideDuty();
        mHomeActivity.hideToolBar();
    }

    PermissionListener permissionListener = new PermissionListener() {
        @Override
        public void onPermissionGranted(PermissionGrantedResponse response) {
            mHomeActivity.startActiveJobs();
        }

        @Override
        public void onPermissionDenied(PermissionDeniedResponse response) {
            mHomeActivity.startActiveJobs();
        }

        @Override
        public void onPermissionRationaleShouldBeShown(PermissionRequest permission,
                                                       PermissionToken token) {
            token.continuePermissionRequest();
        }
    };
}
