package com.agero.ncc.fragments;

import android.location.Address;
import android.os.AsyncTask;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TimePicker;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.model.TowLocation;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class AddTowDestinationFragment extends BaseFragment {
    HomeActivity mHomeActivity;
    Unbinder unbinder;
    @BindView(R.id.edit_tow_dest_name)
    EditText editTowDestinationName;
    @BindView(R.id.edit_tow_dest)
    EditText editTowDestination;
    @BindView(R.id.edit_tow_city)
    EditText editTowCity;
    @BindView(R.id.edit_tow_zip)
    EditText editTowZip;
    @BindView(R.id.spinner_tow_state)
    Spinner spinnerTowState;
    @BindView(R.id.radioButton_night_drop_off_no)
    RadioButton rb_nightDropNo;
    @BindView(R.id.radioButton_night_drop_off_yes)
    RadioButton rb_nightDropYes;
    TowLocation towDestination;
    @BindView(R.id.radioButton_storage_no)
    RadioButton rb_storageNo;
    @BindView(R.id.radioButton_storage_yes)
    RadioButton rb_storageYes;
    @BindView(R.id.edit_facility_time)
    EditText faciltyTimeView;
    @BindView(R.id.edit_facility_time_two)
    EditText faciltyTimeView2;
    @BindView(R.id.edit_estimated_time)
    EditText estimatedArrivalTime;



    public AddTowDestinationFragment() {
        // Intentionally empty
    }

    public static AddTowDestinationFragment newInstance(boolean isAddNew) {
        AddTowDestinationFragment fragment = new AddTowDestinationFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(NccConstants.BUNDLE_KEY_ADD_NEW, isAddNew);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_add_tow_destination, fragment_content, true);
        unbinder = ButterKnife.bind(this, view);
        mHomeActivity = (HomeActivity) getActivity();
        mHomeActivity.hideBottomBar();
        mHomeActivity.showToolbar(getString(R.string.title_add_tow));
        if (getArguments().getBoolean(NccConstants.BUNDLE_KEY_ADD_NEW)) {
            towDestination = new TowLocation();
        } else {
            editTowDestinationName.setText(AddJobsDetailsFragment.newJob.getTowLocation().getName());
            editTowDestination.setText(AddJobsDetailsFragment.newJob.getTowLocation().getAddressLine1());
            editTowCity.setText(AddJobsDetailsFragment.newJob.getTowLocation().getCity());
            editTowZip.setText(AddJobsDetailsFragment.newJob.getTowLocation().getPostalCode());
        }

        return superView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.button_add_tow_save, R.id.radioButton_night_drop_off_no, R.id.radioButton_night_drop_off_yes, R.id.radioButton_storage_no, R.id.radioButton_storage_yes,
            R.id.edit_facility_time, R.id.edit_facility_time_two, R.id.edit_estimated_time})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button_add_tow_save:

                if (editTowDestination.getText().toString().isEmpty() || editTowCity.getText().toString().isEmpty()) {
                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_TOAST(), new UserError("", "", "Address fields shouldn't be empty"));
                } else if (spinnerTowState.getSelectedItemPosition() == 0) {
                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_TOAST(), new UserError("", "", "Please select one of the States"));
                } else {
                    new GeoCodingTowDestinationTask().execute("" + editTowDestination.getText().toString() + "," +
                            editTowCity.getText().toString() + "," + spinnerTowState.getSelectedItem().toString());
                }
                break;
            case R.id.radioButton_night_drop_off_no:
                rb_nightDropYes.setChecked(false);
                towDestination.setNightDropOff(false);
                break;
            case R.id.radioButton_night_drop_off_yes:
                rb_nightDropNo.setChecked(false);
                towDestination.setNightDropOff(true);
                break;
            case R.id.radioButton_storage_no:
                rb_storageYes.setChecked(false);
                AddJobsDetailsFragment.newJob.getTowLocation().setIsTowToStorage(false);
                break;
            case R.id.radioButton_storage_yes:
                rb_storageNo.setChecked(false);
                AddJobsDetailsFragment.newJob.getTowLocation().setIsTowToStorage(true);
                break;
            case R.id.edit_facility_time:
                showTimePicker(view);
                break;
            case R.id.edit_facility_time_two:
                showTimePicker(view);
                break;
            case R.id.edit_estimated_time:
                showTimePicker(view);
                break;


        }
    }

    private void showTimePicker(final View view ){
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(this.getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {


                String am_pm = "";
                Calendar datetime = Calendar.getInstance();
                datetime.set(Calendar.HOUR_OF_DAY, selectedHour);
                datetime.set(Calendar.MINUTE, selectedMinute);

                if (datetime.get(Calendar.AM_PM) == Calendar.AM)
                    am_pm = "AM";
                else if (datetime.get(Calendar.AM_PM) == Calendar.PM)
                    am_pm = "PM";

                String strHrsToShow = (datetime.get(Calendar.HOUR) == 0) ?"12":datetime.get(Calendar.HOUR)+"";
                if(view instanceof EditText ){
//                    AddJobsDetailsFragment.newJob.getTowDestination()
                    ((EditText) view).setText(strHrsToShow+":"+datetime.get(Calendar.MINUTE)+" "+am_pm);
                }

            }
        }, hour, minute, false);
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }


    private class GeoCodingTowDestinationTask extends AsyncTask<String, Integer, Address> {

        @Override
        protected Address doInBackground(String... strings) {
            return Utils.getGeoCodedAddress(getActivity(), strings[0]);
        }

        @Override
        protected void onPostExecute(Address address) {
            if (address != null) {

                TowLocation towDestination = new TowLocation();
                towDestination.setLandmark(editTowDestinationName.getText().toString());
                towDestination.setAddressLine1(editTowDestination.getText().toString());
                towDestination.setCity(editTowCity.getText().toString());
                towDestination.setState(spinnerTowState.getSelectedItem().toString());
                towDestination.setPostalCode(editTowZip.getText().toString());
                towDestination.setLatitude(address.getLatitude());
                towDestination.setLongitude(address.getLongitude());

                if (getArguments().getBoolean(NccConstants.BUNDLE_KEY_ADD_NEW)) {
                    AddJobsDetailsFragment.newJob.setTowLocation(towDestination);
                    mHomeActivity.push(AddVehicleDetailsFragment.newInstance(true), getString(R.string.title_add_vehicle));
                } else {
                    AddJobsDetailsFragment.newJob.setTowLocation(towDestination);
                    mHomeActivity.onBackPressed();
                }
            } else {
                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_TOAST(), new UserError("", "", getString(R.string.add_job_detail_enter_valid_address)));
            }
        }
    }

}
