package com.agero.ncc.model

import android.net.Uri
import android.os.Parcelable
import com.google.firebase.database.Exclude
import kotlinx.android.parcel.Parcelize

data class JobDetail(
        var acceptedById: String? = null, //1234
        var acceptedByName: String? = null, //Lester Santos
        var acceptedDateTime: String? = null, //2017-12-14T20:31:58.665106
        var batchNumber: Int? = 0,
        var callReason: String? = null, //Flat Tire
        var callReasonSummary: String? = null,
        var capturedVinInfo: CapturedVinInfo? = null,
        var comments: List<Comment>? = null,
        var clientId: Long? = 0,
        var coverage: String? = null, //Covered to listed location.
        var currentStatus: Status? = null,
        var customerCallbackNumber: String? = null, //593 556 0233
        var customerName: String? = null, //Maggie Carpenter
        var customerPays: String? = null, //Cash
        var cancelReasonDescription: String? = null,
        var disablementLocation: DisablementLocation? = null,
        var dispatchAssignedToId: String? = null, //l36QDA0HySHuEojtZQjuCFjVWclpvm
        var dispatchAssignedToName: String? = null, //l36QDA0HySHuEojtZQjuCFjVWclpvm
        var dispatchCreatedDate: String? = null, //null
        var dispatchDate: String? = null, //2017-09-15T16:50:15
        var dispatchId: String? = null, //4522194999
        var dispatchSource: String? = null, //phone
        var dispatchTraceId: String? = null,
        var documents: Any? = null, //null
        var driverAssignedById: String? = null, //74473
        var driverAssignedByName: String? = null, //Lester Santos
        //var driverId: String? = null, //Lester Santos
        var equipmentList: List<EquipmentItem>? = null,
        var etaDateTime: String? = null,
        var etaExtensionCount: Int? = 0,
        var etaStatus: String? = null,
        var extenuationCircumstances: List<ExtenuationCircumstancesItem>? = null,
        var facilityId: String? = null, //af9c5806-8b78-41a7-ba75-51b954ccb757
        var invoiceAmount: Float? = 0F, //632
        var costBreakDown: Cost? = null,
//        var isCustomerWithVehicle: Boolean = false, //false
        @get:JvmName("getIsCustomerWithVehicle") @set:JvmName("setIsCustomerWithVehicle") var isCustomerWithVehicle: Boolean? = false,
        var isStorage: Boolean? = false, //true
        var isTowToStorage: Boolean? = false, //null
        var milesToDisablementLoc: Float = 0F, //9
        var milesToTowLoc: Float? = 0F, //40
        var deadHeadMiles: Float? = 0F, //40
        var modifyDateTime: String? = null,
        var offerAcceptedByFacilitiesCount: Int? = 0,
        var offerId: String? = null, //OfferId
        var offeredToFacilitiesCount: Int? = 0,
        var paymentMethod: String? = null, //CC
        var personInPossessionOfVehicle: String? = null, //Lester Santos
        var poNumber: String? = null, //2647504135
        var policyNumber: String? = null,
        var partnerFacility: PartnerFacility? = null,
        var proposedEta: Int? = 0, //90
        var quotedEta: Int? = 0, //90
        var receivedTime: String? = null, //2017-09-15T16:50:15
        var refusedReasonCode: String? = null, //2017-09-15T16:50:15
        var refusedReasonText: String? = null, //2017-09-15T16:50:15
        var releaseRequirements: String? = null, //IpDxnaa3wXRjqqemqqKO5jIeqGaGgl5si5BKnpri
        var reportAttachment: VehicleReportModel? = null,
        var segment: Int? = 0,
//        var service: String? = null, //Tow
        var serviceTypeCode: String? = null,
        var statusHistory: ArrayList<Status>? = null,
        var storageHours: String? = null, //8:00am to 5:00pm
        var timestamp: String? = null, //2017-09-15T16:50:15
        var totalAdvanceCharges: Float? = 0F, //$444
//        var totalEtaInMinutes: Int? = 0,
        var towLocation: TowLocation? = null,
        var vehicle: Vehicle? = null,
        var vehicleDamageInformation: String? = null, //Front rear door damaged
        var vinCaptureRequired: Boolean? = false, //false
        var totalOfferCountInBatch: Int? = 0,
        var totalEtaInMinutes: Int? = 0,
        var totalSegments: Int? = 0,
        var etaChangeHistory: ArrayList<ETA>? = null,
        var disablementLocationHistory: ArrayList<DisablementLocation>? = null,
        var towLocationHistory: ArrayList<TowLocation>? = null,
        var jobType: String? = null,
        var carrier : String? = null,
        var claimNumber : String? = null,
        var clientName : String? = null,
        var damageInformation : String? = null,
        var eligibilityCheckRequired : Boolean? = false
)

data class Cost (
        var nightCost: Float? = 0F,
        var sundayCost: Float? = 0F
)



data class Status(
        var appName: String? = null, //n9TDHBafNnWj
        var appId: String? = null, //n9TDHBafNnWj
        var serverTimeUtc: String? = null, //2017-09-15T16:50:15
        var statusCode: String? = null, //OnScene
        var statusTime: String? = null, //2017-09-15T16:50:15
        var subSource: String? = null, //xb0G6dnxRmQ5DJa
        var userId: String? = null, //1234
        var userName: String? = null, //Lester Santos
        var deleted: Boolean? = false, //Lester Santos
        var source: String? = null, //Lester Santos
        var reason: String? = null,
//        var isPending: Boolean? = false,
        @get:JvmName("getIsPending") @set:JvmName("setIsPending") var isPending: Boolean? = false,
        @get:JvmName("getIsJobOnHold") @set:JvmName("setIsJobOnHold") var isJobOnHold: Boolean? = false,
        var deviceId: String? = null,
        var reasonCode: String? = null,
        var userAction: String? = null,
        var totalEtaInMinutes: Int? = null,
        var statusChangeRequestedBy:String? = null,
        @get:JvmName("getIsGeofence") @set:JvmName("setIsGeofence") var isGeofence: Boolean? = false
)


data class EquipmentItem(
        var equipmentCode: String? = null,
        var equipmentTypeCode: String? = null,
        var equipmentTypeName: String? = null
)

data class ETA(
        var appId: String? = null,
        var deviceId: String? = null,
        var appName: String? = null,
        var etaInMinutes: Int? = 0,
        var isApproved: Boolean? = false,
        var reasonCode: String? = null,
        var reasonText: String? = null,
        var serverTimeUtc: String? = null,
        var source: String? = null,
        var subSource: String? = null,
        var statusCode: String? = null,
        var userId: String? = null,
        var userName: String? = null
)

data class CapturedVinInfo(
        var captureTime: String? = null, //2017-09-15T16:50:15
        var subSource: String? = null, //A0634VlsuH
        var vin: String? = null //TFOEWUCNN
)

data class Comment(
        var appId: String? = null,
        var deviceId: String? = null,
        var appName: String? = null,
        var userId: String? = null,
        var userName: String? = null,
        var commentText: String? = null,
        var commentType: String? = null,
        var serverTimeUtc: String? = null,
        var source: String? = null,
        var subSource: String? = null
)

data class DisablementLocation(
        var addressLine1: String? = null, //1 Locust Ave
        var addressLine2: String? = null, //null
        var city: String? = null, //Warwick
        var county: String? = null, //USA
        var country: String? = null, //USA
        var crossStreet: String? = null, //null
        var garageClearanceFeet: Int? = 0,
        var garageClearanceInches: Int? = 0,
        var garageInfo: String? = null, //Wu8ORCkJpgSX7d5hVMiMFE
        var garageLevel: Int? = 0,
        var garageSection: Any? = null,
        var landmark: String? = null, //null
        var locationType: String? = null,
        var latitude: Double? = null, //41.7007728
        var longitude: Double? = null, //-71.4539157
        var name: String? = null, //JOSEPH  OTOOLE
        var nightDropOff: Boolean? = false, //After 6:00pm
        var phoneNumber: String? = null, //5297648743
        var poi: String? = null, //null
        var postalCode: String? = null, //63244
        var state: String? = null, //RI
        var stateProvince: String? = null, //s4
        var signature: Signature? = null,
        var appId: String? = null, //n9TDHBafNnWj
        var serverTimeUtc: String? = null, //2017-09-15T16:50:15
        var statusTime: String? = null, //2017-09-15T16:50:15
        var subSource: String? = null, //xb0G6dnxRmQ5DJa
        var userId: String? = null, //1234
        var userName: String? = null, //Lester Santos
        var deviceId: String? = null,
        var source: String? = null, //Lester Santos
        var status: String? = null,
        var timeZone: String? = null,
        var timeZoneCode: String? = null,
        var timeZoneName: String? = null,
        var requestId: String? = null,
        var stockLotNumber: String? = null

)

data class TowLocation(
        var addressLine1: String? = null, //1300 PONTIAC AVE
        var addressLine2: String? = null, //null
        var city: String? = null, //CRANSTON
        var county: String? = null, //CRANSTON
        var country: String? = null, //USA
        var crossStreet: String? = null, //null
        var garageClearanceFeet: Int? = 0,
        var garageClearanceInches: Int? = 0,
        var garageInfo: String? = null, //Wu8ORCkJpgSX7d5hVMiMFE
        var garageLevel: Int? = 0,
        var garageSection: Any? = null,
        var landmark: String? = null, //null
        var latitude: Double? = null, //41.748
        var longitude: Double? = null, //-71.4518
        var locationType: String? = null,
        var name: String? = null, //v3UgrF5dV9ps
        var nightDropOff: Boolean? = false, //After 6:00pm
        @get:JvmName("getIsTowToStorage") @set:JvmName("setIsTowToStorage") var isTowToStorage: Boolean? = false,
        var phoneNumber: String? = null, //2875282689
        var poi: String? = null, //null
        var postalCode: String? = null, //82438
        var state: String? = null, //RI
        var stateProvince: String? = null, //6V
        var signature: Signature? = null,
        var appId: String? = null, //n9TDHBafNnWj
        var serverTimeUtc: String? = null, //2017-09-15T16:50:15
        var statusTime: String? = null, //2017-09-15T16:50:15
        var subSource: String? = null, //xb0G6dnxRmQ5DJa
        var userId: String? = null, //1234
        var userName: String? = null, //Lester Santos
        var deviceId: String? = null,
        var source: String? = null, //Lester Santos
        var status: String? = null,
        var stockLotNumber: String? = null
)

data class VehicleReportModel(
        var notes: String? = null,
        var addedDate: String? = null,
        var documents: List<VehicleReportModelEntity>? = null
)

data class ExtenuationCircumstancesItem(
        var extenuationDetailText: String? = null,
        var extenuationType: String? = null
)

@Parcelize
data class VehicleReportModelEntity(
        var type: String? = null,
        var localUri: Uri? = null,
        var docUrl: String? = null,
        var thumbUrl: String? = null,
        var tags: ArrayList<String>? = null,
        var videoLength: Long? = null) : Parcelable


data class Vehicle(
        var vehicleId: Int? = 0, //124587
        var color: String? = null, //White
        var driveTrainType: String? = null, //Front Wheel Drive
        var driveTrainTypeCode: String? = null, //FWD
        var fuelType: String? = null, //Hybrid
        var fuelTypeCode: String? = null, //null
        var make: String? = null, //Volvo
        var mileage: String? = null, //null
        var model: String? = null, //S80
        var plate: String? = null, //PA-MAGPIE
        var state: String? = null, //DD
        var vehicleType: String? = null, //Passenger Car
        var vehicleTypeCode: String? = null, //PC
        var numberOfPassengers: Int? = null, //null
        var vin: String? = null, //TFOEWUCNN
        var year: Int? = 0 //2000
)

data class Signature(
        var reason: String? = null,
        var signeeName: String? = null,
        var signatureUrl: String? = null
)

data class PartnerFacility(
        var addressLine1: String? = null,
        var addressLine2: String? = null,
        var addressName: String? = null,
        var addressTypeCd: String? = null,
        var afterHoursFlg: Boolean? = false,
//        var afterHoursPhoneNumber: Boolean? = false,
        var apartmentSuiteNumber: String? = null,
        var city: String? = null,
        var contactName: String? = null,
        var countryCd: String? = null,
        var crossStreet1: String? = null,
        var crossStreet2: String? = null,
        var jobPreference: String? = null,
        var latitude: Double? = 0.0,
        var level: String? = null,
        var longitude: Double? = 0.0,
        var phoneNumber: String? = null,
        var postalCd: String? = null,
        var serviceRadius: Int? = 0,
        var showCostToDispatcherFlg: Boolean? = false,
        var stateCd: String? = null,
        var timeZone: String? = null
        )