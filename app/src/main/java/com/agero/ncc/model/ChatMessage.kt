package com.agero.ncc.model

data class ChatMessage(
    var fromId: String,
    var fromUserName: String,
    var toId: String,
    var toUserName: String,
    var timeStamp: Long,
    var message: String
) {
  constructor() : this("1234", "", "5678", "", 23, "Hi")
}