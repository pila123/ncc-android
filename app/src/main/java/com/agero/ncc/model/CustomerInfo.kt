package com.agero.ncc.model

/**
 * Created by sdevabhaktuni on 2/8/18.
 */


data class CustomerInfoModel(
        var customerProfile: CustomerProfile? = null,
        var callerDetails: CallerDetails? = null
)


data class CustomerProfile(
        var profilePostalCd: String? = null, //1234
        var unListedPersonFlag: Boolean? = false, //1234
        var profileStateCd: String? = null, //1234
        var callerId: String? = null, //1234
        var vipFlag: Boolean? = false, //1234
        var profileLastname: String? = null, //1234
        var profileCity: String? = null, //1234
        var profileFirstName: String? = null, //1234
        var profileId: String? = null //1234
)


data class CallerDetails(
        var profilePostalCd: String? = null, //1234
        var callerFirstName: String? = null, //1234
        var callerStateCd: String? = null, //1234
        var callerAddress1: String? = null, //1234
        var callerPostalCd: String? = null, //1234
        var callerLastName: String? = null, //1234
        var profileSourceId: String? = null, //1234
        var profileId: Long? = 0, //1234
        var clientEmployeeFlag: Boolean? = false //1234
)