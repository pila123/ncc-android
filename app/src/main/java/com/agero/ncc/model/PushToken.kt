package com.agero.ncc.model

data class PushToken(
        var token: String,
        var platform: String
)