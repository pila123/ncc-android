package com.agero.ncc.model.vehicledetails

data class MyVehicleDetails (
    var makesCount: Int = 0,
    var makes: List<MakesItem>? = null
)

data class YearsItem(
        var year: Int = 0,
        var id: Int = 0
)

data class ModelsItem(
        var name: String? = null,
        var id: String? = null,
        var niceName: String? = null,
        var years: List<YearsItem>? = null
)

data class MakesItem(
        var models: List<ModelsItem>? = null,
        var name: String? = null,
        var id: Int = 0,
        var niceName: String? = null
)