package com.agero.ncc

import android.util.Log
import android.widget.Toast
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

//    @Test
//    fun printFunction() {
//        var arrayInt = Array<String>(5) { "" }
//        arrayInt[0] = "123"
//        for (item in arrayInt) {
//            System.out.println(item)
//
//        }
//       var booleanValue = if (10 > 20) true else nothingFun()
//        sumAddition(10, 20)
//        sumAddition(10)
//        sumAddition()
//
//    }
//
//    fun sumAddition(n1: Int = 0, n2: Int = 0) {
//        println(n1 + n2)
//    }
//
//    fun nothingFun(): Nothing {
//        println("nothing function")
//        throw IllegalArgumentException()
//    }

//    @Test
//    fun oopFunction(){
//        var t1 = Test1(10,20)
//        var t2 = Test2(10,20,30)
//        var t2new = Test2(10,20)
//        var t12 = Test2(10,20,30) as Test1
//
//        t1.sum()
//        t1.display()
//        t2.sum()
//        t2.display()
//        t2.display2()
//        t2new.sum()
//        t2new.display()
//        t2new.display2()
//        t12.sum()
//        t12.display()
//    }

//    open class Test1{
//
//        var a1: Int? = null
//        var b1: Int? = null
//
//        constructor(a: Int, b: Int){
//            this.a1 = a
//            this.b1 = b
//            println("Cons Test1")
//        }
//
//        constructor(a: Int, b: Int, c: Int){
//            this.a1 = a
//            this.b1 = b
//        }
//
//        init {
//            println("init Test1")
//        }
//
//        open fun sum() {
//            println("sum: ($a1+$b1) = "+(a1!!+b1!!))
//        }
//
//        fun display() {
//            println("a:b $a1 : $b1")
//        }
//    }
//
//    open class Test2(var a: Int, var b: Int, var c: Int) : Test1(a,b){
//
////        var a:Int? = null
////        var b:Int? = null
////        var c:Int? = null
//
////        constructor(a: Int,  b: Int,  c: Int):super(a,b){
////            this.a = a
////            this.b = b
////            this.c = c
////            println("Cons Test2")
////        }
//
//        constructor(a: Int,  b: Int):this(a,b,10){
//            this.a = a
//            this.b = b
//            this.c = c
//            println("Cons Test2")
//        }
//
//        override fun sum() {
//            println("sum: ($a+$b+$c) = "+(a!!+b!!+c!!))
//        }
//
//        fun display2() {
//            println("a:b:c $a : $b : $c")
//        }
//
//        init {
//            println("init Test2")
//        }
//    }

    interface abc{
        fun test1()
        fun test2(){println("interface Test2 interface")}
        fun test3(){println("interface Test3 interface")}
    }
    class InterChecker:abc{
        override fun test1() {
            println("interface Test1 class")
        }

        override fun test2() {
            println("interface Test2 class")
        }

    }
    @Test
    fun interfaceTest(){
        var icheck = InterChecker()
        icheck.test1()
        icheck.test2()
        icheck.test3()
    }
}
